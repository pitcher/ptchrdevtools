#PTCHRDEVTOOLS


## Bugherd

#### Installing
Use 
` {!! Pitcher/Bugherd::render() !!}` in the footer to load Bugherd when using blade.

### Requirements
Place `BUGHERD_API_KEY` in your .env file with the corresponding api key for this project.

### Rules
This script will only be inserted when the current site is a staging environment.
