<?php
/**
 * Created by PhpStorm.
 * User: Pitcher - HP
 * Date: 21/08/2020
 * Time: 11:08
 */

namespace PtchrProjects\PtchrDevTools\ImgSrcSet;

use Samrap\Acf\Acf;
use StoutLogic\AcfBuilder\FieldsBuilder;

/**
 * Class Options
 * @package PtchrProjects\PtchrDevTools\ImgSrcSet
 */
class Options
{

    /**
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    static function setup()
    {
        add_action('acf/init', function () {
            self::setupOption();
        });
    }

    /**
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    private static function setupOption()
    {
        $field = new FieldsBuilder('Fallback Image');
        $field->addImage('fallbackimage',
            [
                'label' => "Fallback afbeelding",
                'instructions' => "Vul hier een algemene fallback afbeelding in. Een fallback image wordt gebruikt in gevallen waarin er tijdens het vullen van de pagina (nog) geen afbeelding is gevuld.",
                'return_format' => 'id'
            ]);


        $field->setLocation('options_page', '==', 'ptchr-settings');

        acf_add_local_field_group($field->build());
    }
    /**
     * @param $name
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     * Creates a new option in which a fallback image can be defined.
     */
    public static function addFallBackImage($name,$return_format = "id")
    {
        $field = new FieldsBuilder('Fallback Image voor ' . $name);
        $field->addImage('fallbackimage_' . $name,
            [
                'label' => "Fallback afbeelding voor " . $name,
                'instructions' => "Vul hier een fallback afbeelding voor " . $name . " in. Een fallback image wordt gebruikt in gevallen waarin er tijdens het vullen van de pagina (nog) geen afbeelding is gevuld.",
                'return_format' => $return_format
            ]);

        $field->setLocation('options_page', '==', 'ptchr-settings');

        acf_add_local_field_group($field->build());
    }

    /**
     * @param bool $name
     * @return mixed
     * @throws \Samrap\Acf\Exceptions\BuilderException
     */
    public static function getFallBackImage($name = false)
    {
        if (!$name || $name == "default") {
            return Acf::option('fallbackimage')->get();
        }


        return Acf::option('fallbackimage_' . $name)->get();

    }
}
