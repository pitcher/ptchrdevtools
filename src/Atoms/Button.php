<?php

namespace PtchrProjects\PtchrDevTools\Atoms;

use PtchrProjects\PtchrDevTools\Atomic\Atom;
use PtchrProjects\PtchrDevTools\Functions;
use PtchrProjects\PtchrDevTools\Lorem;
use Samrap\Acf\Acf;

class Button extends Atom
{
    protected $fieldname = 'button';
    protected $url = '#';
    protected $target = '';
    protected $title = 'lorem';
    protected $iconbefore = '';
    protected $iconafter = '';
    protected $warningclass = '';
    protected $size = '';
    protected $style = 'primary';
    protected $classes = [];

    public function __construct()
    {
        parent::__construct();
        $this->setName('button');
    }

    /**
     * @return string
     */
    public function getFieldname(): string
    {
        return $this->fieldname;
    }

    /**
     * @param string $fieldname
     * @return Button
     */
    public function setFieldname(string $fieldname): self
    {
        $this->fieldname = $fieldname;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return Button
     */
    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return string
     */
    public function getTarget(): string
    {
        return $this->target;
    }

    /**
     * @param string $target
     * @return Button
     */
    public function setTarget(string $target): self
    {
        $this->target = $target;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Button
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getIconbefore(): string
    {
        return $this->iconbefore;
    }

    /**
     * @param string $iconbefore
     * @return Button
     */
    public function setIconbefore(string $iconbefore): self
    {
        $this->iconbefore = $iconbefore;

        return $this;
    }

    /**
     * @return string
     */
    public function getIconafter(): string
    {
        return $this->iconafter;
    }

    /**
     * @param string $iconafter
     * @return Button
     */
    public function setIconafter(string $iconafter): self
    {
        $this->iconafter = $iconafter;

        return $this;
    }

    /**
     * @return string
     */
    public function getWarningclass(): string
    {
        return $this->warningclass;
    }

    /**
     * @param string $warningclass
     * @return Button
     */
    public function setWarningclass(string $warningclass): self
    {
        $this->warningclass = $warningclass;

        return $this;
    }

    /**
     * @return string
     */
    public function getSize(): string
    {
        return $this->size;
    }

    /**
     * @param string $size
     * @return Button
     */
    public function setSize(string $size): self
    {
        $this->size = $size;

        return $this;
    }

    /**
     * @return array
     */
    public function getClasses(): array
    {
        return $this->classes;
    }

    /**
     * @param array $classes
     * @return Button
     */
    public function setClasses(array $classes): self
    {
        $this->classes = $classes;

        return $this;
    }

    public function validate()
    {
        if ($this->url == '#' || ! $this->url):
            $this->setWarningclass("warning");
        endif;
    }

    /**
     * @return string
     */
    public function getStyle(): string
    {
        return $this->style;
    }

    /**
     * @param string $style
     * @return Button
     */
    public function setStyle(string $style): self
    {
        $this->style = $style;

        return $this;
    }

    public function setManual($array = false)
    {
        if (! $array) {
            return false;
        }

        if (! is_object($array)) {
            $array = Functions::arrayToObject($array);
        }

        if (isset($array->url)) {
            $this->setUrl($array->url);
        }

        if (isset($array->target)) {
            $this->setTarget($array->target);
        }

        if (isset($array->title)) {
            $this->setTitle($array->title);
        }

        return $this;
    }

    public function retrieveDataFromAcf()
    {
        $acfdata = Acf::field($this->getName())->default(Lorem::loremButton($this->title))->get();
        if (! is_object($acfdata)) {
            $acfdata = Functions::arrayToObject($acfdata);
        }
        if (isset($acfdata->url)) {
            $this->setUrl($acfdata->url);
        }
        if (isset($acfdata->target)) {
            $this->setTarget($acfdata->target);
        }
        if (isset($acfdata->title)) {
            $this->setTitle($acfdata->title);
        }
    }

    public function get($skipretrieve = false)
    {
        if (! $skipretrieve) {
            $this->retrieveDataFromAcf();
        }
        $this->validate();

        return $this;
    }
}
