<?php

namespace PtchrProjects\PtchrDevTools;


class Bugherd
{
    static function render()
    {
        if(getenv('BUGHERD_API_KEY') && (getenv('WP_ENV') == 'staging') ){
            echo '<script type="text/javascript" src="https://www.bugherd.com/sidebarv2.js?apikey='.getenv('BUGHERD_API_KEY').'" async="true"></script>';
        }
    }
}
