<?php
/**
 * Created by PhpStorm.
 * User: Pitcher - HP
 * Date: 31/07/2020
 * Time: 14:33
 */

namespace PtchrProjects\PtchrDevTools\Lorem;


use Samrap\Acf\Acf;
use StoutLogic\AcfBuilder\FieldsBuilder;

class Options
{
    static function setup()
    {
        self::setupLoremIpsumOption();
    }

    private static function setupLoremIpsumOption()
    {
        $field = new FieldsBuilder('Lorem Ipsum Switch');
        $field->addSelect('loremipsum_setting',
            [
                'label' => "Zet dit aan als je gebruik wil maken van een placeholder Lorem Ipsum teksten. Dit is handig tijdens het vullen",

            ])->addChoices(
            [
                'default' => "Gebruik standaard instellingen (aan op dev & staging)",
                'blocks' => "Gebruik Blocksum Ipsum standaard instellingen, ( aan op dev & staging)",
                'false' => "Forceer Uit, ook op dev en staging",
                'true' => " Forceer aan, op alle omgevingen, inc. productie"
            ]);


        $field->setLocation('options_page', '==', 'ptchr-settings');

        acf_add_local_field_group($field->build());
    }

    public static function getLoremSetting()
    {
        return Acf::option('loremipsum_setting')->get();
    }
}
