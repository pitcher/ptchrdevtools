<?php

namespace PtchrProjects\PtchrDevTools;

use function App\getId;
use PtchrProjects\PtchrDevTools\Fields\Text;
use Samrap\Acf\Acf;
use StoutLogic\AcfBuilder\FieldsBuilder;

abstract class Block
{

    /**
     * @var string
     */
    protected $type = '';

    /**
     * @var string
     */
    protected $name = '';

    /**
     * @var string
     */
    protected $nicename = '';

    /**
     * @var string
     */
    protected $location = '';

    /**
     * @var array
     */
    protected $fields = [];

    /**
     * @var string
     */
    protected $classes = "";

    /**
     * @var array
     */
    private $fieldSet = [];

    /**
     * @var array
     */
    private $infofieldSet = [];


    private $basicmode = false;

    /**
     * @return bool
     */
    public function isBasicmode(): bool
    {
        return $this->basicmode;
    }

    /**
     * @param bool $basicmode
     * @return Block
     */
    public function setBasicmode(bool $basicmode): Block
    {
        $this->basicmode = $basicmode;
        return $this;
    }

    /**
     * @var array
     */
    private $optionFieldSet = [];

    protected $dataTheme = '';
    protected $dataShapeTop;
    protected $dataShapeBottom;
    protected $sectionClass = '';
    protected $sectionId = '';
    protected $ColumnClass = '';
    protected $preferredTemplate = '';
    protected $blocktype = 'section';

    private $iconContent = "&#128221;";
    private $iconSettings = "&#128295;";
    private $iconInfo = "&#128173;";

    /**
     * Block constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->setName($name);
        $this->processCMSOptions();
        $this->setType('block');

        $this->setDefaultFields();

        $this->setup();
    }

    abstract public function setup();

    /**
     * @param object $field
     */
    protected function set(object $field)
    {
        $this->fields[] = $field;
        $this->fieldSet[] = $field;
    }

    protected function setOption(object $field)
    {
        $this->fields[] = $field;
        $this->optionFieldSet[] = $field;
    }

    protected function setInfo(object $field)
    {
        $this->fields[] = $field;
        $this->infofieldSet[] = $field;
    }

    function setDefaultFields()
    {
        $this->applyAnchorFields();

    }

    function applyAnchorFields()
    {
        if (!env('DISABLE_BLOCK_ANCHOR')) {
            $anchor = (new Text('anchor', "Plaats hier een verankering in het blok.",
                "Je kan vervolgens met #[ankertekst] navigeren naar dit blok. Laat leeg om hier geen gebruik van te maken. Spaties worden niet ondersteund."))->setDefaultActive(0);
            $this->setOption($anchor);
        }
    }

    function getAnchor()
    {
        return Acf::field('anchor')->get();
    }


    /**
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    public function debug()
    {
        echo "<h3>Dit blok bevat de volgende data:</h3>";
    }

    /**
     * @return object
     * This function can be overwritten in your own custom class, data retrieval will be manual in that case.
     */
    public function get()
    {
        if ($this->isBasicmode()) {
            return get_fields();
        }

        $return = [];

        foreach ($this->fields as $field) {
            if ($field->type !== 'hidden') {
                $field->prepare();
                $return[$field->name] = $field->retrieveField();
            }
        }

        return Functions::arrayToObject($return);
    }

    /**
     * @return mixed
     */
    public function getBlocktype()
    {
        return $this->blocktype;
    }

    /**
     * @param mixed $blocktype
     * @return Block
     */
    public function setBlocktype($blocktype)
    {
        $this->blocktype = $blocktype;

        return $this;
    }

    /**
     * @param $name
     * @param $location
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function build($name, $location, $nicename = false)
    {

        $this->nicename = $nicename ? $nicename : $name;
        $this->name = $name;
        $this->location = strtolower($location);
        $block = new FieldsBuilder($this->name);
        $block->addMessage('📏 ' . $this->nicename . ' 📏', '');
        if (is_array($this->fieldSet) && count($this->fieldSet) > 0) {
            $block->addtab($this->iconContent);
            foreach ($this->fieldSet as $field) {
                $block->addFields($field->build());
            }
        }

        if (is_array($this->optionFieldSet) && count($this->optionFieldSet) > 0) {
            $block->addtab($this->iconSettings);
            foreach ($this->optionFieldSet as $field) {
                $block->addFields($field->build());
            }
        }

        if (is_array($this->infofieldSet) && count($this->infofieldSet) > 0) {
            $block->addTab($this->iconInfo);
            foreach ($this->infofieldSet as $field) {
                $block->addFields($field->build());
            }
        }

        $block->setLocation('block', '==', 'acf/' . $this->location);
        if (function_exists('acf_add_local_field_group')) {
            acf_add_local_field_group($block->build());
        }
    }

    /**
     * @return mixed
     */
    public function getDataShapeTop()
    {
        return $this->dataShapeTop;
    }

    /**
     * @return mixed
     */
    public function getDataShapeBottom()
    {
        return $this->dataShapeBottom;
    }

    /**
     * @return mixed
     */
    public function getFormattedDataShapeTop()
    {
        if ($shape = $this->getDataShapeTop()) {
            return 'data-shape-top="' . $shape . '"';
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getFormattedDataShapeBottom()
    {
        if ($shape = $this->getDataShapeBottom()) {
            return 'data-shape-bottom="' . $shape . '"';
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getDataTheme()
    {
        return $this->dataTheme;
    }

    /**
     * @return mixed
     */
    public function getFormattedDataTheme()
    {
        if ($theme = $this->getDataTheme()) {

            if ($theme == "default") {
                return false;
            }
            if ($theme == "theme") {
                return "data-theme";
            }

            return 'data-theme="' . $this->getDataTheme() . '"';

        }

        return false;
    }

    /**
     * @param mixed $dataTheme
     * @return Block
     */
    public function setDataTheme($dataTheme)
    {
        $this->dataTheme = $dataTheme;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSectionClass()
    {
        return $this->sectionClass;
    }

    /**
     * @param mixed $sectionClass
     * @return Block
     */
    public function setSectionClass($sectionClass)
    {
        $this->sectionClass = $sectionClass;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSectionID()
    {
        return $this->sectionId;
    }

    public function getFormattedSectionId()
    {
        if ($this->getSectionID()) {
            return 'id="' . $this->getSectionID() . '"';
        }

        return false;
    }

    /**
     * @param mixed $sectionID
     * @return Block
     */
    public function setSectionID($sectionID)
    {
        $this->sectionId = $sectionID;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getColumnClass()
    {
        return $this->ColumnClass;
    }

    /**
     * @param mixed $ColumnClass
     */
    public function setColumnClass($ColumnClass): void
    {
        $this->ColumnClass = $ColumnClass;
    }

    /**
     * @return mixed
     */
    public function getPreferredTemplate()
    {
        return $this->preferredTemplate;
    }

    /**
     * @param mixed $preferredTemplate
     * @return Block
     */
    public function setPreferredTemplate($preferredTemplate)
    {
        $this->preferredTemplate = $preferredTemplate;

        return $this;
    }


    /**
     * @throws \Samrap\Acf\Exceptions\BuilderException
     */
    public function processCMSOptions()
    {
        $this->dataShapeBottom = Acf::field('dataShapeBottom')->get();
        $this->dataShapeTop = Acf::field('dataShapeTop')->get();

        //var_dump($this->dataShapeTop, $this->dataShapeBottom);

        $this->setDataTheme(Acf::field('theme')->default('default')->get());
        $this->setSectionID(Acf::field('id')->default(false)->get());

    }

    /**
     * @return mixed
     */
    public function getName()
    {
        if ($this->nicename) {
            return $this->nicename;
        }

        if ($this->name) {
            return $this->name;
        }

        return false;
    }

    /**
     * @param mixed $name
     * @return Block
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return Block
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getClasses()
    {
        return $this->classes;
    }

    /**
     * @param mixed $class
     * @return Block
     */
    public function setClass($class)
    {
        $this->classes = $class;

        return $this;
    }

    /**
     * @param mixed $class
     * @return Block
     */
    public function addClass($class)
    {
        $this->classes .= " ". $class;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFormattedClasses()
    {
        if (!$this->getClasses()) {
            return '';
        }
        $classes = '';

        if (is_array($this->getClasses())) {
            $classes = '';
            foreach ($this->getClasses() as $class) {
                $classes .= ' ' . $class;
            }
        }

        if (!is_array($this->getClasses())) {
            $classes = $this->getClasses();
        }

        return 'class="' . $classes . '"';
    }


}
