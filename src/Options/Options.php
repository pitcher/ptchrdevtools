<?php
/**
 * Created by PhpStorm.
 * User: Pitcher - HP
 * Date: 31/07/2020
 * Time: 14:34
 */

namespace PtchrProjects\PtchrDevTools\Options;


class Options
{
    static function setupoptions()
    {
        /*
         * create a Global options page
         */

        if (function_exists('acf_add_options_page')) {
            acf_add_options_page(array(
                'page_title' => __('Site Instellingen'),
                'menu_title' => __('Site Instellingen'),
                'menu_slug' => 'ptchr-settings',
                'capability' => 'edit_posts',
                'redirect' => false,
                'autoload' => true,
            ));
        }

        /*
         * Add a Lorem Ipsum setting switch
         */
        \PtchrProjects\PtchrDevTools\Lorem\Options::setup();


//        /*
//         * Add a override for default image
//         */
        \PtchrProjects\PtchrDevTools\ImgSrcSet\Options::setup();

        self::Krumo();
    }

    static function Krumo()
    {
        if (env('WP_ENV') !== "development") {
            \krumo::disable();
        }
    }
}
