<?php

namespace PtchrProjects\PtchrDevTools\Resources;


class DefaultTermFilterResource extends Resource
{
    function Mapping($posts)
    {
        return array_map(function ($post) {
            return [
                'id' => $post->term_id,
                'name' => $post->name,
                'count' => $post->count
            ];

        }, $posts);
    }
}
