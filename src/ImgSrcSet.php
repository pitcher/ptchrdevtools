<?php

namespace PtchrProjects\PtchrDevTools;


/**
 * Get Image Source Set.
 *
 * @param $id
 * @param $size
 * @param $fit
 * @param $class
 *
 * @return
 */
class ImgSrcSet
{
    public static function getImageSourceSet($id, $size = 'full', $fit = 'cover', $class = '')
    {
        if(!$id){
            return false;
        }

        if (!function_exists('wp_get_attachment_image')) {
            return false;
        }

        if (!function_exists('get_field')) {
            return false;
        }

        $srcset = wp_get_attachment_image(
            $id,
            $size,
            false,
            ['class' => 'img']);

        if(!$srcset){
            return false;
        }

        $focalpoint = '50% 50%';

        if (get_field('focus', $id)) {
            $focalpoint = get_field('focus', $id);
        }

        $focalpointff = 'object-fit:' . $fit . '; object-position:center center ';
        $focusoutput = '<figure class="object-' . $fit . ' ' . $class . '" loading="lazy"><img style="object-position:' . $focalpoint . '; font-family:\'' . $focalpointff . '\'"';

        $srcset = str_replace('<img', $focusoutput, $srcset);
        $srcset = $srcset . '</figure>';

        return $srcset;
    }

    public static function getImageUrl($id)
    {
        if (function_exists('wp_get_attachment_url')) {
            return wp_get_attachment_url($id);
        }
        return false;

    }

    public static function convertImageToSourceSet($url, $fit = 'cover', $class = '')
    {
        $focalpoint = '50% 50%';
        $focalpointff = 'object-fit:' . $fit . '; object-position:center center ';
        return '<figure class="object-' . $fit . ' ' . $class . '"><img src="'.$url.'" style=" object-position:' . $focalpoint . '; font-family:\'' . $focalpointff . '\'"></figure>';



    }
}
