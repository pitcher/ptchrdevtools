<?php

namespace PtchrProjects\PtchrDevTools;


class Initialize
{
    public function __construct()
    {
        new GutenbergCategories();
        new HeadingHelper();
        new BlockEditorStyling();
        new removeDashboardSiteHealthWidget();
    }
}
