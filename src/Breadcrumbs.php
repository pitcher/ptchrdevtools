<?php

namespace PtchrProjects\PtchrDevTools;

use function App\getId;

class Breadcrumbs
{

    static $homePage = [];

    static $nodes = [];

    static $currentPage = [];

    static function build()
    {
        self::setHomePage();
        self::setCurrentPage();

        // We know the current page, and we know the homepage:
        // Homepage
        //      ?.?.?.?
        //          Current page
        // We need to determine what steps are between the homepage and the currentpage
        // these steps can be custom post types, categories, taxonomies(?) or one or more nested pages/posts/cpts.
        // to connect the homepage to the currentpage we need to navigate our way back from the current page to the home page
        self::setNodes();
    }

    static function getType($pageid)
    {

        if(is_tax($pageid)){
            return "taxonomy";
        }

        $p = get_post($pageid);

        if(get_post_type($pageid) == 'acf-field' || get_post_type($pageid) == 'acf-field-group'){
            return false;
        }

        if (isset($p->type) && $p->type == 'post') {

            return "post";
            //Do the stuff with post
        }

        if (get_post_type($pageid)) {
            return "page";
            //Do the stuff with page
        }


        return false;


    }


    static function setHomePage()
    {
        $id = get_option('page_on_front');

        self::$homePage = [
            'id' => $id,
            'title' => get_the_title($id),
            'url' => get_the_permalink($id)
        ];
    }

    static function setCurrentPage()
    {
        $id = Functions::getId();



        if(is_tax()){

            $term = get_queried_object();

            self::$currentPage = [
                'id' => $id,
                'title' => $term->name,
                'url' => get_term_link($term),
                'type' => 'taxonomy'
            ];
        }else{
            self::$currentPage = [
                'id' => $id,
                'title' => get_the_title($id),
                'url' => get_the_permalink($id),
                'type' => 'page',
            ];
        }
    }

    static function checkParentStructure($id)
    {
        $parentid = wp_get_post_parent_id($id);

        if ($parentid) {
            // we add the current page to the nodes, but only if its a known type
            $type = self::getType($id);

            if($type){
                $data = [
                    'id' => $parentid,
                    'title' => get_the_title($parentid),
                    'url' => get_the_permalink($parentid),
                    'type' => $type
                ];


                self::addNode($data);

                self::checkParentStructure($parentid); // RECURSION
            }

        }

    }

    static function addNode(array $node)
    {
        array_unshift(self::$nodes, $node);
    }

    static function setNodes()
    {
        // we now know the currentpage, and we know the homepage.
        // we need to determine what kind of page this is, so we prepare all checks
        $customPostType = get_post_type(self::$currentPage['id']);

        // Check parent structure
        self::checkParentStructure(self::$currentPage['id']);

        $type = self::getType(self::$currentPage['id']);

        //scenario custom post type
        if ($customPostType
            && is_singular($customPostType)
            && $customPostType !== 'page'
            && $customPostType !== "acf-field") {

            // then we add $custom post type to the nodes
            $customPostTypeObject = get_post_type_object($customPostType);

            $title = $customPostTypeObject->label;
            $url = get_post_type_archive_link($customPostType);

            if ($acfNode = get_field($customPostType . 'archivepage', 'options')) {
                $title = $acfNode['title'];
                $url = $acfNode['url'];
            }



            self::addNode([
                'id' => '',
                'title' => $title,
                'url' => $url,
                'type' => $type
            ]);
        }
//        //scenario Taxonomy
//        if (is_tax()) {
//            $term = get_term(get_queried_object()->term_id);
//            self::addNode([
//                'id' => $term->term_id,
//                'title' => $term->name,
//                'url' => get_term_link($term),
//                'type' => 'taxonomy'
//            ]);
//        }

    }

    static function get()
    {
        // Setup all data
        self::build();

        //Sometimes the currentpage is the homepage, in that case return nothing.
        if (self::$homePage['id'] == self::$currentPage['id']) {
            return false;
        }

        // return formatted data
        return [
            'homePage' => self::$homePage,
            'nodes' => self::$nodes,
            'currentPage' => self::$currentPage
        ];

    }
}
