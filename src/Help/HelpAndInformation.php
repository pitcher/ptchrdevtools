<?php


namespace PtchrProjects\PtchrDevTools\Help;


use StoutLogic\AcfBuilder\FieldsBuilder;

class HelpAndInformation
{
    function __construct()
    {
        $this->setupHelpPage();
        add_action('acf/init', function () {
            $this->addBlockInformation();
        });

    }

    function setupHelpPage()
    {
        if (function_exists('acf_add_options_page')) {
            acf_add_options_page(array(
                'page_title' => __('Help en Informatie'),
                'menu_title' => __('Help en Informatie'),
                'menu_slug' => 'ptchr-help',
                'capability' => 'edit_posts',
                'icon_url' => "dashicons-editor-help",
                'redirect' => false,
                'autoload' => true,
            ));
        }
    }

    function addBlockInformation()
    {
        // Global $sage_error so we can throw errors in the typical sage manner
        global $sage_error;

        // Get an array of directories containing blocks
        $directories = apply_filters('sage-acf-gutenberg-blocks-templates', []);
        $blockhelp = new FieldsBuilder('Blokkenoverzicht');
        $blockhelp->setLocation('options_page', '==', 'ptchr-help');
//        $blockhelp->addTab('blokken');
        // Check whether ACF exists before continuing
        foreach ($directories as $dir) {
            // Sanity check whether the directory we're iterating over exists first
            if (!file_exists(\locate_template($dir))) {
                return;
            }
            // Iterate over the directories provided and look for templates
            $template_directory = new \DirectoryIterator(\locate_template($dir));

            foreach ($template_directory as $template) {
                if (!$template->isDot() && !$template->isDir()) {

                    // Strip the file extension to get the slug
                    $slug = removeBladeExtension($template->getFilename());
                    // If there is no slug (most likely because the filename does
                    // not end with ".blade.php", move on to the next file.

                    if (!$slug) {
                        continue;
                    }

                    // Get header info from the found template file(s)
                    $file_path = locate_template($dir . "/${slug}.blade.php");

                    $file_headers = get_file_data($file_path, [
                        'title' => 'Title',
                        'description' => 'Description',
                        'category' => 'Category',
                        'icon' => 'Icon',
                        'keywords' => 'Keywords',
                        'mode' => 'Mode',
                        'align' => 'Align',
                        'post_types' => 'PostTypes',
                        'supports_align' => 'SupportsAlign',
                        'supports_anchor' => 'SupportsAnchor',
                        'supports_mode' => 'SupportsMode',
                        'supports_multiple' => 'SupportsMultiple',
                        'enqueue_style' => 'EnqueueStyle',
                        'enqueue_script' => 'EnqueueScript',
                        'enqueue_assets' => 'EnqueueAssets',
                    ]);

                    $title = $file_headers['title'];

                    if(!$title){
                        continue;
                    }
                    $description = $file_headers['description'];

                    $assetdir = get_template_directory() . "/help/blocks/";
                    $publicdir = get_template_directory_uri() .  "/help/blocks/";

                    $filename = $slug.".png";

                    if(file_exists($assetdir.$filename)){
                        $description .= '<br><br><br><img src="'.$publicdir.$filename.'" alt="">';
                    }else{
                        if(env('WP_ENV') == "development"){
                            $description .= '<br><br><br><img src="'.$publicdir.'default.png" alt="">';
                            $description .= "<br> <br> requested image at ". $assetdir.$filename . " not found.";
                        }
                    }



                    $blockhelp->addMessage("blokinformatie: " . $title, $description);


//                    $blockhelp->addMessage($title, [
//                        'label' => 'Message Field',
//                        'instructions' => '',
//                        'required' => 0,
//                        'conditional_logic' => 0,
//                        'wrapper' => [
//                            'width' => '',
//                            'class' => '',
//                            'id' => '',
//                        ],
//                        'message' => '',
//                        'new_lines' => 'wpautop',
//                        'esc_html' => 0,
//                    ]);
                }
            }
        }

        acf_add_local_field_group($blockhelp->build());
    }
}
/**
 * Function to strip the `.blade.php` from a blade filename
 */
function removeBladeExtension($filename)
{
    // Filename must end with ".blade.php". Parenthetical captures the slug.
    $blade_pattern = '/(.*)\.blade\.php$/';
    $matches = [];
    // If the filename matches the pattern, return the slug.
    if (preg_match($blade_pattern, $filename, $matches)) {
        return $matches[1];
    }
    // Return FALSE if the filename doesn't match the pattern.
    return FALSE;
}
