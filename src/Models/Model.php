<?php

namespace PtchrProjects\PtchrDevTools\Models;


use StoutLogic\AcfBuilder\FieldsBuilder;

/**
 * Class Model
 * @package App\Models\Base
 */
abstract class Model
{
    /**
     * @var string
     */
    public $type = "";

    /**
     * @var string
     */
    public $nicescription = "";

    /**
     * @var string
     */
    public $singlename = "";

    /**
     * @var string
     */
    public $pluralname = "";

    /**
     * @var string
     */
    public $slug = "";

    /**
     * @var array
     */
    public $args = [];

    /**
     * @var array
     */
    public $labels = [];

    /**
     * @var array
     */
    public $model = [];

    /**
     * @var array
     */
    public $template = [];

    /**
     * @var bool
     */
    public $template_lock = false;

    /**
     * @var array
     */
    private $fieldSet = [];

    /**
     * @var array
     */
    protected $fields = [];


    /**
     * @return array
     */
    public function getTemplateLock(): bool
    {
        return $this->template_lock;
    }

    /**
     * @param bool $template_lock
     * @return Model
     */
    public function setTemplateLock(bool $template_lock): Model
    {
        $this->template_lock = $template_lock;
        return $this;
    }

    /**
     * @return array
     */
    public function getTemplate(): array
    {
        return $this->template;
    }

    /**
     * @param array $template
     * @return Model
     */
    public function setTemplate(array $template): Model
    {
        $this->template = $template;
        return $this;
    }

    /**
     *
     */
    public function __construct()
    {
        //setup default args.
        $this->args = array(
            'label' => __($this->pluralname, 'ptchr-theme'),
            'labels' => $this->labels,
            'supports' => array('title', 'editor', 'revisions'),
            'taxonomies' => array(),
            'hierarchical' => false,
            'public' => true,
            'menu_icon' => '',
            'show_ui' => true,
            'show_in_menu' => true,
            'menu_position' => 1,
            'show_in_admin_bar' => true,
            'show_in_nav_menus' => true,
            'can_export' => true,
            'has_archive' => true,
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'show_in_rest' => true,
            'capability_type' => 'page',
            'rewrite' => array('slug' => __($this->slug)),
        );

        $this->labels = array(
            'name' => _x($this->pluralname, 'Post Type General Name', 'ptchr-theme'),
            'singular_name' => _x($this->singlename, 'Post Type Singular Name', 'ptchr-theme'),
            'menu_name' => __($this->pluralname, 'ptchr-theme'),
            'name_admin_bar' => __($this->pluralname, 'ptchr-theme'),
            'archives' => __($this->pluralname . ' archief', 'ptchr-theme'),
            'attributes' => __($this->pluralname . ' attributen', 'ptchr-theme'),
            'parent_item_colon' => __($this->singlename . ' item', 'ptchr-theme'),
            'all_items' => __('Alle ' . $this->pluralname, 'ptchr-theme'),
            'add_new_item' => __('Voeg een ' . $this->singlename . ' toe', 'ptchr-theme'),
            'add_new' => __('Voeg een nieuwe ' . $this->singlename . ' toe', 'ptchr-theme'),
            'new_item' => __('Nieuwe ' . $this->pluralname, 'ptchr-theme'),
            'edit_item' => __('Bewerk ' . $this->singlename, 'ptchr-theme'),
            'update_item' => __('Update ' . $this->singlename, 'ptchr-theme'),
            'view_item' => __('Bekijk ' . $this->singlename, 'ptchr-theme'),
            'view_items' => __('Bekijk ' . $this->singlename, 'ptchr-theme'),
            'search_items' => __('Zoek pagina', 'ptchr-theme'),
            'not_found' => __('Not found', 'ptchr-theme'),
            'not_found_in_trash' => __('Not found in Trash', 'ptchr-theme'),
            'featured_image' => __('Featured Image', 'ptchr-theme'),
            'set_featured_image' => __('Set featured image', 'ptchr-theme'),
            'remove_featured_image' => __('Remove featured image', 'ptchr-theme'),
            'use_featured_image' => __('Use as featured image', 'ptchr-theme'),
            'insert_into_item' => __('Insert into item', 'ptchr-theme'),
            'uploaded_to_this_item' => __('Uploaded to this item', 'ptchr-theme'),
            'items_list' => __('Items list', 'ptchr-theme'),
            'items_list_navigation' => __('Items list navigation', 'ptchr-theme'),
            'filter_items_list' => __('Filter items list', 'ptchr-theme'),
        );
    }

    /**
     *
     */
    public function create_custom_post_type()
    {
        register_post_type($this->type, $this->args);

    }

    public function setupTemplate()
    {
        $post_type_object = get_post_type_object($this->getType());
        $post_type_object->template = $this->getTemplate();
        $post_type_object->template_lock = $this->getTemplateLock();

    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getNicescription(): string
    {
        return $this->nicescription;
    }

    /**
     * @param string $nicescription
     */
    public function setNicescription(string $nicescription): void
    {
        $this->nicescription = $nicescription;
    }

    /**
     * @return string
     */
    public function getSinglename(): string
    {
        return $this->singlename;
    }

    /**
     * @param string $singlename
     */
    public function setSinglename(string $singlename): void
    {
        $this->singlename = $singlename;
    }

    /**
     * @return string
     */
    public function getPluralname(): string
    {
        return $this->pluralname;
    }

    /**
     * @param string $pluralname
     */
    public function setPluralname(string $pluralname): void
    {
        $this->pluralname = $pluralname;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return array
     */
    public function getArgs(): array
    {
        return $this->args;
    }

    /**
     * @param array $args
     */
    public function setArgs(array $args): void
    {
        $this->args = $args;
    }

    /**
     * @return array
     */
    public function getLabels(): array
    {
        return $this->labels;
    }

    /**
     * @param array $labels
     */
    public function setLabels(array $labels): void
    {
        $this->labels = $labels;
    }

    /**
     * @param object $field
     */
    function set(object $field)
    {
        $this->fields[] = $field;
        $this->fieldSet[] = $field->build();
    }

    /**
     * @param $aside
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function build($position = 'side')
    {
        $cptFields = new FieldsBuilder($this->getType(),   ['title' => $this->getNicescription() . " Opties", 'position' => $position, 'menu_order' => 0]);

        $cptFields->addAccordion($this->getNicescription() . " Opties");

        foreach ($this->fieldSet as $field) {
            $cptFields->addFields($field);
        }

        $cptFields->setLocation('post_type', '==', $this->getType());
        if (function_exists('acf_add_local_field_group')) {
            acf_add_local_field_group($cptFields->build());
        }
    }



}
