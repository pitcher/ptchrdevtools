<?php

namespace PtchrProjects\PtchrDevTools;

class Functions
{
    static function arrayToObject($array)
    {
        if (is_string($array)) {
            return $array;
        }

        if (is_object($array)) {
            return (object)$array;
        }

        if (!is_array($array)) {
            return $array;
        }

        $obj = (object)[];
        foreach ($array as $k => $v) {
            if (strlen($k)) {
                if (is_array($v)) {
                    $obj->{$k} = self::arrayToObject($v); //RECURSION
                } else {
                    $obj->{$k} = $v;
                }
            }
        }

        return $obj;
    }

    static function isAdmin()
    {
        if (!function_exists('is_admin')) {
            return false;
        }

        return is_admin();
    }

    static function is_not_production($data = "")
    {
        if (!function_exists('env')) {
            return false;
        }

        if (env('WP_ENV') == 'production') {
            return false;
        }

        if (env('WP_ENV') !== 'production') {
            if ($data) {
                return $data;
            }

            return true;
        }
        return false;
    }


    static function my_acf_post_id()
    {
        return self::getId();
    }

    static function getId()
    {

        if(is_tax()){
            return get_queried_object()->term_id;
        }


        if (is_admin() && function_exists('acf_maybe_get_POST')) :
            return intval(acf_maybe_get_POST('post_id'));
        else :
            global $post;

            if ($post !== null) {
                return $post->ID;
            } else {
                if (isset(get_queried_object()->term_id)) {
                    return get_queried_object()->term_id;
                }
            }

        endif;

        return intval(acf_maybe_get_POST('post_id'));

    }

}
