<?php

namespace PtchrProjects\PtchrDevTools\Fields;


use Samrap\Acf\Acf;
use StoutLogic\AcfBuilder\FieldsBuilder;

class File extends BaseField
{
    /**
     * @var string
     */
    public $type = "file";

    public function __construct($name = "file", $label = "Bestand", $instructions = "", $required = 0,$default = false)
    {
        parent::__construct($name, $label, $instructions, $required, $default);
        $this->name = $name;
        $this->label = $label;
        $this->instructions = $instructions;
        $this->required = $required;
    }

    public function build()
    {
        $file = new FieldsBuilder($this->getName());
        $file->addFile($this->getName(), [
            'label' => $this->getLabel(),
            'instructions' => $this->getInstructions(),
            'required' => $this->getRequired(),
            'conditional_logic' => [],
            'wrapper' => [
                'width' => $this->getWidth()? $this->getWidth() : 40,
                'class' => '',
                'id' => '',
            ],
            'return_format' => 'array',
            'library' => 'all',
            'min_size' => '',
            'max_size' => '',
            'mime_types' => '',
        ]);

        return $file;
    }
    /**
     * @return string
     */
    public function render(): string
    {
        return false;
    }
}



