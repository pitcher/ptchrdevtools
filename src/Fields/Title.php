<?php

namespace PtchrProjects\PtchrDevTools\Fields;


use PtchrProjects\PtchrDevTools\HeadingHelper;
use PtchrProjects\PtchrDevTools\Lorem;
use Samrap\Acf\Acf;
use StoutLogic\AcfBuilder\FieldsBuilder;

/**
 * Class Title.
 */
class Title extends BaseField
{
    /**
     * @var string
     */
    public $type = 'title';

    /**
     * @var integer
     */
    public $preferredHeadingSize = 1;

    /**
     * @return int
     */
    public function getPreferredHeadingSize(): int
    {
        return $this->preferredHeadingSize;
    }

    /**
     * @param int $preferredHeadingSize
     * @return Title
     */
    public function setPreferredHeadingSize(int $preferredHeadingSize): Title
    {
        $this->preferredHeadingSize = $preferredHeadingSize;
        return $this;
    }

    /**
     * @return string
     */
    public function getHeadingclass(): string
    {
        return $this->headingclass;
    }

    /**
     * @param string $headingclass
     * @return Title
     */
    public function setHeadingclass(string $headingclass): Title
    {
        $this->headingclass = $headingclass;
        return $this;
    }

    /**
     * @var string
     */
    public $headingclass = '';

    public function __construct(
        $name = 'title',
        $label = 'Titel',
        $instructions = "",
        $required = false,
        $default = false,
        $preferredHeadingSize = 1,
        $headingclass = ""
    ) {

        parent::__construct($name, $label, $instructions, $required, $default);

        if(!$default){
            $this->setDefault(Lorem::loremipsum('short'));
        }

        $this->preferredHeadingSize = $preferredHeadingSize;
        $this->headingclass = $headingclass;
    }

    /**
     * @return mixed|FieldsBuilder
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function build()
    {
        $title = new FieldsBuilder($this->name);
        $title->addText(
            $this->name,
            [
                'label' => $this->getLabel(),
                'instructions' => $this->getInstructions(),
                'required' => $this->getRequired(),
                'wrapper' => [
                    'width' => $this->getWidth()? $this->getWidth() : 50,
                ]
            ]);

        return $title;
    }

    /**
     * @return mixed|string
     * @throws \Samrap\Acf\Exceptions\BuilderException
     */
    public function render(): string
    {
        return $this->format($this->get);
    }

    public function format($data)
    {
        return HeadingHelper::Render($data,
            $this->preferredHeadingSize,
            $this->headingclass
        );
    }

}
