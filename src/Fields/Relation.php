<?php

namespace PtchrProjects\PtchrDevTools\Fields;

use PtchrProjects\PtchrDevTools\Resources\Resource;
use Samrap\Acf\Acf;
use StoutLogic\AcfBuilder\FieldsBuilder;

/**
 * Class Relation.
 */
class Relation extends BaseField
{
    /**
     * @var string
     */
    public $type = 'relation';

    /**
     * @var array
     */
    public $post_type = [];

    /**
     * @var array
     */
    public $taxonomies = [];
    /**
     * @var string
     */
    public $min = '';
    /**
     * @var string
     */
    public $max = '';
    /**
     * @var string
     */
    public $return_format = 'object';

    /**
     * @var array
     */
    public $args = [];

    /**
     * @var array
     */
    public $resource = [];


    /**
     * Relation constructor.
     * @param string $name
     * @param string $label
     * @param string $instructions
     * @param int $required
     * @param array $post_types
     * @param array $default
     * @param array $taxonomies
     * @param string $min
     * @param string $max
     * @param string $return_format
     */
    public function __construct(
        $name = 'relation',
        $label = 'Relatie veld',
        $instructions = '',
        $required = 0,
        $post_type = [],
        $default = [],
        $taxonomies = [],
        $min = '',
        $max = '',
        $return_format = 'object'
    ) {

        parent::__construct($name, $label, $instructions, $required, $default);

        $this->post_type = $post_type;
        $this->taxonomies = $taxonomies;
        $this->min = $min;
        $this->max = $max;
        $this->return_format = $return_format;

        $this->args = [
            'label' => $this->label,
            'instructions' => $this->instructions,
            'required' => $this->required,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => $this->getWidth() ? $this->getWidth() : 40,
                'class' => '',
                'id' => '',
            ],
            'post_type' => $this->post_type,
            'taxonomy' => $this->taxonomies,
            'filters' => [
                0 => 'search',
                1 => 'post_type',
                2 => 'taxonomy',
            ],
            'elements' => '',
            'min' => $this->min,
            'max' => $this->max,
            'return_format' => $this->return_format,
        ];
    }

    public function setResource(Resource $resource)
    {
        $this->resource = $resource;

        return $this;
    }

    /**
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function build()
    {
        $relation = new FieldsBuilder($this->name);
        $relation->addRelationship($this->name, $this->args);

        return $relation;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function format($data)
    {
        return $this->resource->Mapping($data);
    }

}
