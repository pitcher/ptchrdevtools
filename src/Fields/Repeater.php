<?php

namespace PtchrProjects\PtchrDevTools\Fields;


use PtchrProjects\PtchrDevTools\Functions;
use PtchrProjects\PtchrDevTools\Lorem;
use Samrap\Acf\Acf;
use StoutLogic\AcfBuilder\FieldsBuilder;


/**
 * Class Repeater.
 */
class Repeater extends BaseField
{

    /**
     * @var string
     */
    public $type = 'repeater';

    /**
     * @var string
     */
    private $layout = 'block';


    /**
     * Repeater constructor.
     * @param array $subfields
     * @param string $name
     * @param string $label
     * @param bool $default
     */
    public function __construct(
        array $subfields = [],
        $name = 'array',
        $label = 'Een of meerdere elementen',
        $instructions = "",
        $default = false,
        $required = false
    ) {
        parent::__construct($name, $label, $instructions, $required, $default);
        $this->setHasSubfields(true);
        $this->name = $name;
        $this->label = $label;
        $this->subfields = $subfields;
        $this->default = $default;

        $this->setSublayout(new FieldsBuilder($this->name . '_child'));

        foreach ($this->subfields as $sf) {
            $this->sublayout->addFields($sf->build());
        }

    }

    /**
     * @return mixed|FieldsBuilder
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function build()
    {
        $repeater = new FieldsBuilder($this->name);
        $repeater->addRepeater($this->name,
            [
                'label' => $this->label,
                'layout' => $this->layout,
                'instructions' => $this->getInstructions(),
                'required' => $this->getRequired(),
                'wrapper' => [
                    'width' => $this->getWidth() ? $this->getWidth() : 100,
                ]
            ]
        )->addFields($this->sublayout);

        return $repeater;
    }

    public function get($pre = false)
    {
        $fieldname = $this->name;
        if ($pre) {
            $fieldname = $pre . "_" . $fieldname;
        }

        $field = Acf::field($fieldname)->get();


        if (!$field && !$this->isDefaultActive()) {
            return $field;
        }

        if (!$field) {

            if (!$this->default && !$this->isDefaultActive()) {
                $structure = [];
                foreach ($this->subfields as $subfield) {

                    if (
                        $subfield->getType() == "repeater" ||
                        $subfield->getType() == "group"
                    ) {
                        $structure[$subfield->name] = [
                            'type' => $subfield->getType(),
                            'get' => $subfield->getDefault(),
                        ];
                    } else {
                        $structure[$subfield->name] = [
                            'type' => $subfield->getType(),
                            'get' => $subfield->getDefault(),
                            'render' => $subfield->format($subfield->getDefault()),
                        ];
                    }


                }

                return Lorem::loremFieldStructure($structure, 2);
            }
            return $this->default;
        }
        if (!$this->isDefaultActive()) {
            return $field;
        }
        foreach ($field as $rowkey => $row) {
            // REPEATER SUBFIELDS ARRAY

            foreach ($row as $key => $value) {

                // REPEATER SUBFIELD
                $subfield = $this->findSubFieldByName($key);
                if ($subfield) {
                    if (
                        $subfield->getType() == "repeater" ||
                        $subfield->getType() == "group"
                    ) {
                        $field[$rowkey][$key] = Functions::arrayToObject([
                            'type' => $subfield->getType(),
                            'get' => $subfield->get($fieldname . "_" . $rowkey) ? $subfield->get($fieldname . "_" . $rowkey) : $subfield->getDefault()
                        ]);
                    } else {
                        $value = $subfield->get($fieldname . "_" . $rowkey);

                        $field[$rowkey][$key] = Functions::arrayToObject([
                            'type' => $subfield->getType(),
                            'get' => $value ? $value : $subfield->getDefault(),
                            'render' => $subfield->format($value) ? $subfield->format($value) : $subfield->getDefault()
                        ]);
                    }
                }

            }

        }

        return $field;

    }
}
