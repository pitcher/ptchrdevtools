<?php

namespace PtchrProjects\PtchrDevTools\Fields;

use PtchrProjects\PtchrDevTools\Functions;
use PtchrProjects\PtchrDevTools\Lorem;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Samrap\Acf\Acf;

/**
 * Class BaseField.
 */
abstract class BaseField
{

    /**
     * @var
     */
    public $name;
    /**
     * @var
     */
    protected $label;
    /**
     * @var
     */
    protected $instructions;
    /**
     * @var
     */
    protected $required;
    /**
     * @var
     */
    protected $default;
    /**
     * @var
     */
    protected $type;
    /**
     * @var
     */
    public $get;
    /**
     * @var
     */
    public $render;

    /**
     * @var
     */
    public $width;

    /**
     * @return mixed
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * @param mixed $min
     * @return BaseField
     */
    public function setMin($min)
    {
        $this->min = $min;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMax()
    {

        return $this->max;
    }

    /**
     * @param mixed $max
     * @return BaseField
     */
    public function setMax($max)
    {
        $this->max = $max;

        return $this;
    }

    /**
     * @var
     */
    public $min;

    /**
     * @var
     */
    public $max
    ;
    /**
     * @array
     */
    public $hasSubfields = false;

    /**
     * @var array|FieldsBuilder
     */
    public $sublayout = [];

    public $defaultActive = true;

    /**
     * @return array|FieldsBuilder
     */
    public function getSublayout()
    {
        return $this->sublayout;
    }

    /**
     * @param array|FieldsBuilder $sublayout
     * @return BaseField
     */
    public function setSublayout($sublayout)
    {
        $this->sublayout = $sublayout;
        return $this;
    }

    /**
     * @return array
     */
    public function getSubfields(): array
    {
        return $this->subfields;
    }

    /**
     * @param array $subfields
     * @return BaseField
     */
    public function setSubfields(array $subfields): BaseField
    {
        $this->subfields = $subfields;
        return $this;
    }

    /**
     * @var array
     */
    public $subfields = [];

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param mixed $width
     * @return BaseField
     */
    public function setWidth($width)
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return BaseField
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     * @return BaseField
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @param mixed $type
     * @return BaseField
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInstructions()
    {
        return $this->instructions;
    }

    /**
     * @param mixed $instructions
     * @return BaseField
     */
    public function setInstructions($instructions)
    {
        $this->instructions = $instructions;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * @param mixed $required
     * @return BaseField
     */
    public function setRequired($required)
    {
        $this->required = $required;
        return $this;
    }

    /**
     * @param mixed $default
     * @return BaseField
     */
    public function setDefault($default)
    {
        $this->default = $default;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDefaultActive(): bool
    {
        return $this->defaultActive;
    }

    /**
     * @param bool $defaultActive
     * @return BaseField
     */
    public function setDefaultActive(bool $defaultActive): BaseField
    {
        $this->defaultActive = $defaultActive;
        return $this;
    }

    /**
     * BaseField constructor.
     * @param $name
     * @param $label
     * @param $instructions
     * @param $required
     * @param $default
     */
    public function __construct($name, $label, $instructions, $required, $default)
    {
        $this->setName($name);
        $this->setLabel($label);
        $this->setInstructions($instructions);
        $this->setRequired($required);
        $this->setDefault($default);
    }

    /**
     * @return FieldsBuilder
     */
    abstract public function build();

    /**
     * @return string
     */
    public function render()
    {
        if (isset($this->subfields)) {
            return false;
        }
        return $this->format($this->get);


    }

    /**
     * @return mixed
     */
    public function getHasSubfields()
    {
        return $this->hasSubfields;
    }

    /**
     * @param mixed $hasSubfields
     * @return BaseField
     */
    public function setHasSubfields($hasSubfields)
    {
        $this->hasSubfields = $hasSubfields;
        return $this;
    }


    /**
     * @return mixed
     */
    public function get($pre = false)
    {
        // prepare all subfields and return
        if ($pre) {
            $field = Acf::field($pre . "_" . $this->name)->default($this->getDefault())->get();
        }

        if (!$pre) {
            $field = Acf::field($this->name)->default($this->getDefault())->get();
        }


        if (!$this->hasSubfields) {
            return $field;
        }


        if (!$field) {
            if (!$this->default && $this->isDefaultActive()) {
                $structure = [];
                foreach ($this->subfields as $subfield) {


                    $structure[$subfield->name] = [
                        'type' => $subfield->getType(),
                        'data' => $subfield->getDefault(),
                        'render' => $subfield->format($subfield->getDefault())
                    ];


                    $structure[$subfield->name] = [
                        'type' => $subfield->getType(),
                        'data' => $subfield->getDefault(),
                        'render' => $subfield->format($subfield->getDefault())
                    ];
                }
                if ($this->getType() == "group") {
                    return $structure;
                }

                return Lorem::loremFieldStructure($structure, 1);
            }
            return $this->default;
        }

        if ($this->getType() == "repeater") {
            // REPEATER
            foreach ($field as $rowkey => $row) {
                // REPEATER SUBFIELDS ARRAY

                foreach ($row as $key => $value) {

                    // REPEATER SUBFIELD

                    $subfield = $this->findSubFieldByName($key);

                    if ($subfield->getType() == "repeater") {

                        if ($pre) {
                            $subfieldData = $subfield->get($pre . "_" . $this->name . "_" . $rowkey);
                        } else {
                            $subfieldData = $subfield->get($this->name . "_" . $rowkey);
                        }

                        if (!$subfieldData && $this->isDefaultActive()) {
                            // If the field is empty, insert the default value
                            $subfieldData = $subfield->getDefault();
                        }


                        $field[$rowkey][$key] = Functions::arrayToObject([
                            'type' => 'repeater',
                            'get' => $subfieldData
                        ]);


                    } elseif ($subfield->getType() == "group") {

                        if ($pre) {
                            $data = $subfield->get($pre . "_" . $this->name . "_" . $rowkey);
                        } else {
                            $data = $subfield->get($this->name . "_" . $rowkey);
                        }
                        if (!$subfield && $this->isDefaultActive())  {
                            // If the field is empty, insert the default value
                            $data = $subfield->getDefault();
                        }

                        $field[$rowkey][$key] = Functions::arrayToObject([
                            'type' => 'repeater',
                            'get' => $data
                        ]);


                    } else {
                        if ($subfield->getType() !== "hidden") {

                            if (!$value && $this->isDefaultActive()) {
                                // If the field is empty, insert the default value
                                $value = $subfield->getDefault();
                            }
                            $field[$rowkey][$key] = Functions::arrayToObject([
                                'type' => $subfield->getType(),
                                'get' => $value,
                                'render' => $subfield->format($value) // possible recursion
                            ]);

                        }
                    }
                }

            }
        }

    }


    /**
     * @return mixed
     */
    public function getDefault()
    {
        if($this->isDefaultActive()){
            return $this->default;

        }

    }


    /**
     * @param $data
     * @return mixed
     */
    public function format($data)
    {
        return $data;
    }

    /**
     * @return mixed|string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     *
     */
    public function prepare()
    {
        $this->get = $this->get();
        $this->render = $this->format($this->get);
    }

    /**
     * @param bool $parent
     * @return bool|object
     */
    public function retrieveField($parent = false)
    {
        if ($this->getType() == "hidden") {
            return false;
        }
        if ($this->getType() == "repeater") {
            return Functions::ArrayToObject([
                'type' => $this->type,
                'get' => $this->get
            ]);
        }

        if ($this->getType() == "group") {
            return Functions::ArrayToObject([
                'type' => $this->type,
                'get' => $this->get
            ]);
        }
        return Functions::ArrayToObject([
            'type' => $this->type,
            'get' => $this->get,
            'render' => $this->render
        ]);
    }

    /**
     * @param $name
     * @return bool|mixed
     */
    public function findSubFieldByName($name)
    {

        foreach ($this->subfields as $subfield) {
            if ($subfield->name == $name) {
                return $subfield;
            }
        }

        return false;
    }

}
