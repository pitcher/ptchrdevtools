<?php

namespace PtchrProjects\PtchrDevTools\Fields;

use Samrap\Acf\Acf;
use StoutLogic\AcfBuilder\FieldsBuilder;

/**
 * Class Relation.
 */
class Taxonomy extends BaseField
{
    /**
     * @var string
     */
    public $name = 'taxonomy';

    /**
     * @var string
     */
    public $type = 'taxonomy';

    /**
     * @var string
     */
    public $label = 'Taxonomie veld';

    /**
     * @var string
     */
    public $instructions = '';

    /**
     * @var int
     */
    public $required = 0;

    /**
     * @var array
     */
    public $taxonomies = [];

    /**
     * @var array
     */
    public $default = [];

    /**
     * @var string
     */
    public $field_type = '';
    /**
     * @var int|string
     */
    public $allow_null = '';
    /**
     * @var string
     */
    public $min = '';

    /**
     * @var string
     */
    public $max = '';

    /**
     * @var string
     */
    public $return_format = 'object';

    /**
     * @var bool|int
     */
    public $multiple = 0;
    /**
     * @var array
     */
    public $args = [];

    /**
     * Taxonomy constructor.
     * @param string $name
     * @param string $label
     * @param string $instructions
     * @param int $required
     * @param array $taxonomies
     * @param array $default
     * @param string $field_type
     * @param int $allow_null
     * @param string $min
     * @param string $max
     * @param string $return_format
     * @param bool $multiple
     */
    public function __construct(
        $name = 'taxonomy',
        $label = 'Taxonomie',
        $instructions = '',
        $required = 0,
        $taxonomies = [],
        $default = [],
        $field_type = 'checkbox',
        $allow_null = 1,
        $min = '',
        $max = '',
        $return_format = 'object',
        $multiple = false
    ) {
        parent::__construct($name, $label, $instructions, $required, $default);

        $this->field_type = $field_type;
        $this->allow_null = $allow_null;
        $this->taxonomies = $taxonomies;
        $this->min = $min;
        $this->max = $max;
        $this->return_format = $return_format;
        $this->multiple = $multiple;

        $this->args = [
            'label' => $this->label,
            'instructions' => $this->instructions,
            'required' => $this->required,
            'conditional_logic' => [],
            'wrapper' => [
                'width' => $this->getWidth()? $this->getWidth() : 40,
                'class' => '',
                'id' => '',
            ],
            'taxonomy' => $this->taxonomies,
            'field_type' => $this->field_type,
            'allow_null' => $this->allow_null,
            'add_term' => 0,
            'save_terms' => 0,
            'load_terms' => 0,
            'return_format' => $this->return_format,
            'multiple' => $this->multiple,
        ];
    }

    /**
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function build()
    {
        $taxonomy = new FieldsBuilder($this->name);
        $taxonomy->addTaxonomy($this->name, $this->args);

        return $taxonomy;
    }

    /**
     * @return bool
     */
    public function render(): string
    {
        return false;
    }

}
