<?php

namespace PtchrProjects\PtchrDevTools\Fields;

use Samrap\Acf\Acf;
use StoutLogic\AcfBuilder\FieldsBuilder;

/**
 * Class Radio.
 */
class Radio extends BaseField
{
      /**
     * @var string
     */
    public $type = 'radio';
    /**
     * @var array
     */
    private $choices = [];


    /**
     * Radio constructor.
     * @param array $choices
     * @param string $name
     * @param string $label
     */
    public function __construct($name = "radio",
        $label = "Maak een keuze",
        $instructions = "",
        $required = false,
        $default = false)
    {
        parent::__construct($name, $label, $instructions, $required, $default);
    }

    /**
     * @return mixed|FieldsBuilder
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function build()
    {
        $title = new FieldsBuilder($this->name);
        $title->addRadio($this->name,
            [
                'label' => $this->label,
                'wrapper' => [
                    'width' => $this->getWidth()? $this->getWidth() : 33,
                    'class' => '',
                    'id' => '',
                ],
            ]
        )->addChoices($this->choices);

        return $title;
    }


    /**
     * @return array
     */
    public function getChoices(): array
    {
        return $this->choices;
    }

    /**
     * @param array $choices
     * @return Radio
     */
    public function setChoices(array $choices): Radio
    {
        $this->choices = $choices;
        return $this;
    }

    /**
     * @return mixed
     */
    public function render(): string
    {
        return false;
    }

    /**
     * @return mixed
     */
    public function getDefault()
    {
        foreach ($this->choices as $item) {
            return $item;
        }
    }
}
