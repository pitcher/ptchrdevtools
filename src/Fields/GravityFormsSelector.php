<?php

namespace PtchrProjects\PtchrDevTools\Fields;


use StoutLogic\AcfBuilder\FieldsBuilder;
use Samrap\Acf\Acf;


/**
 * Class Radio
 * @package App\Blocks\Fields
 */
class GravityFormsSelector extends BaseField
{

    /**
     * @var string
     */
    public $type = "gravityForms";
    /**
     * @var array
     */
    private $choices = [];


    /**
     * GravityFormsSelector constructor.
     * @param string $name
     * @param string $label
     */

    public function __construct($name = "gravityForms",
        $label = "Kies een formulier",
        $instructions = "Selecteer uw formulier",
        $required = false,
        $default = false)
    {
        parent::__construct($name, $label, $instructions, $required, $default);

        if (class_exists(\GFAPI::class)) {

            foreach (\GFAPI::get_forms() as $form) {
                $this->choices[] [$form['id']] = $form['title'] . " ( #" . $form['id'] . ")";
            }
        }
    }

    /**
     * @return mixed|FieldsBuilder
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function build()
    {
        $gfselector = new FieldsBuilder($this->name);
        $gfselector->addSelect($this->name,
            [
                'label' => $this->label,
                'instructions' => $this->getInstructions(),
                'required' => $this->getRequired(),
                'wrapper' => [
                    'width' => $this->getWidth()? $this->getWidth() : 50,
                    'class' => '',
                    'id' => '',
                ],
                'multiple' => 0,
                'return_format' => 'value',
                'choices' => $this->choices
            ]
        );


        if (class_exists(PtchrGravityFormsExtention::class)) {
            // Our extention plugin exists. Give the user the option to use native gravity forms as fallback

            $gfselector->addTrueFalse('method',
                [
                    'label' => "Kies onderliggende techniek",
                    'instructions' => "U heeft de keuze uit de standaard opzet, of de uitgebreide opzet. De maatwerk opzet 
                    is opgebouwd om 1:1 overeen te komen met het ontwerp, maar kan in sommige gevallen niet geheel 
                    overweg met custom plugins die gravity forms uitbreiden. Als u zelf een formulier toevoegt wat 
                    afwijkt van de standaard velden dan kiest u 'standaard', anders selecteert u 'uitgebreid'.",
                    'wrapper' =>
                        [
                            'width' => '50'
                        ],
                    'ui' => 1,
                    'ui_on_text' => 'uitgebreid',
                    'ui_off_text' => 'standaard',
                    'default_value' => 0,

                ]);
        }


        return $gfselector;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getDefault()
    {
        return [
            'form_id' => false,
            'extended_method' => false,
        ];
    }

    public function format($data)
    {
        return [
            'form_id' => Acf::field($this->name)->get(),
            'extended_method' => Acf::field('method')->default(false)->get()
        ];
    }
}
