<?php

namespace PtchrProjects\PtchrDevTools\Fields;

use PtchrProjects\PtchrDevTools\Lorem;
use Samrap\Acf\Acf;
use StoutLogic\AcfBuilder\FieldsBuilder;


/**
 * Class TextArea.
 */
class TextArea extends BaseField
{
    /**
     * @var string
     */
    public $type = 'textarea';

    public $newlines = "br";

    /**
     * TextArea constructor.
     * @param string $name
     * @param string $loremipsumsetting
     */
    public function __construct(
        $name = 'textarea',
        $label = 'Tekst',
        $instructions = "",
        $required = false,
        $default = false)
    {
        parent::__construct($name, $label, $instructions, $required, $default);

        if(!$default){
            $this->setDefault(Lorem::loremipsum('text'));
        }
    }

    /**
     * @return mixed|FieldsBuilder
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function build()
    {
        $title = new FieldsBuilder($this->name);
        $title->addTextarea(
            $this->name,
            [
                'label' => $this->label,
                'instructions' => $this->getInstructions(),
                'required' => $this->getRequired(),
                'maxlength' => $this->getMax(),
                'wrapper' => [
                    'width' => $this->getWidth() ? $this->getWidth() : 60,
                ],
                'new_lines' => $this->getNewLines()
            ]);

        return $title;
    }

    /**
     * @return bool
     */
    public function render(): string
    {
        return false;
    }


    /**
     * @return string
     */
    public function getNewlines(): string
    {
        return $this->newlines;
    }

    /**
     * @param string $newlines
     * @return TextArea
     */
    public function setNewlines(string $newlines): TextArea
    {
        $this->newlines = $newlines;
        return $this;
    }
}

