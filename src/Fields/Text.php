<?php

namespace PtchrProjects\PtchrDevTools\Fields;

use PtchrProjects\PtchrDevTools\Lorem;
use Samrap\Acf\Acf;
use StoutLogic\AcfBuilder\FieldsBuilder;

/**
 * Class Text.
 */
class Text extends BaseField
{

    /**
     * @var string
     */
    public $type = 'text';

    /**
     * Text constructor.
     * @param string $name
     * @param string $label
     * @param string $instructions
     * @param bool $required
     * @param bool $default
     */
    public function __construct(
        $name = 'text',
        $label = 'Tekst',
        $instructions = "",
        $required = false,
        $default = false
    ) {
        parent::__construct($name, $label, $instructions, $required, $default);

        if(!$default){
            $this->setDefault(Lorem::loremipsum('short'));
        }
    }

    /**
     * @return FieldsBuilder
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function build()
    {
        $title = new FieldsBuilder($this->name);
        $title->addText(
            $this->name,
            [
                'label' => $this->label,
                'instructions' => $this->instructions,
                'required' => $this->required,
                'min' => $this->getMin(),
                'max' => $this->getMax(),
                'wrapper' => [
                    'width' => $this->getWidth()? $this->getWidth() : 60,
                ]
            ]);

        return $title;
    }

    /**
     * @return string
     */
    public function render(): string
    {
        return false;
    }
}
