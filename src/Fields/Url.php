<?php

namespace PtchrProjects\PtchrDevTools\Fields;

use Samrap\Acf\Acf;
use StoutLogic\AcfBuilder\FieldsBuilder;


/**
 * Class Title.
 */
class Url extends BaseField
{
    /**
     * @var string
     */
    public $type = 'url';


    public function __construct(
        $name = 'url',
        $label = 'URL',
        $instructions = "",
        $required = false,
        $default = false
    ) {
        parent::__construct($name, $label, $instructions, $required, $default);
        if(!$default){
            $this->setDefault("#");
        }
    }


    /**
     * @return mixed|FieldsBuilder
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function build()
    {
        $url = new FieldsBuilder($this->getName());
        $url->addUrl(
            $this->getName(),
            [
                'label' => $this->getLabel(),
                'instructions' => $this->getInstructions(),
                'required' => $this->getRequired(),
                'wrapper' => [
                    'width' => $this->getWidth()? $this->getWidth() : 50,
                ]
            ]);

        return $url;
    }

    /**
     * @return mixed|string
     * @throws \Samrap\Acf\Exceptions\BuilderException
     */
    public function render(): string
    {
       return false;
    }
}
