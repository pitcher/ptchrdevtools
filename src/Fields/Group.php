<?php

namespace PtchrProjects\PtchrDevTools\Fields;

use PtchrProjects\PtchrDevTools\Functions;
use Samrap\Acf\Acf;
use StoutLogic\AcfBuilder\FieldsBuilder;


/**
 * Class Group.
 */
class Group extends BaseField
{

    /**
     * @var string
     */
    public $type = 'group';

    /**
     * @var string
     */
    private $layout = 'block';


    /**
     * Group constructor.
     * @param array $subfields
     * @param string $name
     * @param string $label
     * @param bool $default
     */
    public function __construct(
        array $subfields = [],
        $name = 'group',
        $label = 'Groep',
        $instructions = "",
        $default = false,
        $required = false
    ) {
        parent::__construct($name, $label, $instructions, $required, $default);
        $this->setHasSubfields(true);
        $this->name = $name;
        $this->label = $label;
        $this->subfields = $subfields;
        $this->default = [];
        $this->sublayout = new FieldsBuilder($this->name . '_child');

        foreach ($this->subfields as $sf) {
            $this->sublayout->addFields($sf->build());
        }
    }



    /**
     * @return mixed|FieldsBuilder
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function build()
    {
        $group = new FieldsBuilder($this->name);
        $group->addGroup($this->name,
            [
                'label' => $this->label,
                'layout' => $this->layout,
                'instructions' => $this->getInstructions(),
                'required' => $this->getRequired(),
                'wrapper' => ['width' => $this->getWidth()]
            ]
        )->addFields($this->sublayout);

        return $group;
    }

    public function get($pre = false)
    {
        $fieldname = $this->name;
        if ($pre) {
            $fieldname = $pre . "_" . $fieldname;
        }

        $field = Acf::field($fieldname)->get();



        if (!$field) {
            if (!$this->default) {
                $structure = [];
                foreach ($this->subfields as $subfield) {
                    $structure[$subfield->name] = [
                        'type' => $subfield->getType(),
                        'get' => $subfield->getDefault(),
                        'render' => $subfield->format($subfield->getDefault()),
                    ];
                }

                return $structure;
            }
            return $this->default;
        }


        foreach ($field as $subfieldkey => $subfieldvalue) {
            // Group SUBFIELDS

            $subfield = $this->findSubFieldByName($subfieldkey);

            $data =  $subfieldvalue ? $subfieldvalue : $subfield->getDefault();


            $field[$subfieldkey] = Functions::arrayToObject([
                'type' => $subfield->getType(),
                'get' => $data,
                'render' => $subfield->format($data)
            ]);


        }


        return $field;

    }



}
