<?php

namespace PtchrProjects\PtchrDevTools\Fields;

use StoutLogic\AcfBuilder\FieldsBuilder;

/**
 * Class Message.
 */
class Message extends BaseField
{
    /**
     * @var string
     */
    public $name = 'message';
    /**
     * @var string
     */
    public $label = 'Uitleg';
    /**
     * @var string
     */
    public $message = 'Bericht';
    /**
     * @var string
     */
    public $type = 'hidden';

    /**
     * Message constructor.
     * @param string $label
     * @param string $message
     * @param string $name
     */
    public function __construct($label = 'Uitleg', $message = 'Bericht', $name = 'message')
    {
        $this->name = $name;
        $this->message = $message;
        $this->label = $label;
    }

    /**
     * @return mixed|FieldsBuilder
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function build()
    {
        $message = new FieldsBuilder($this->name);
        $message->addMessage($this->label, $this->message);

        return $message;
    }

    /**
     * @return bool|mixed
     */
    public function render(): string
    {
        return false;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return bool|mixed
     */
    public function get($pre = false)
    {
        return false;
    }

    /**
     * @return bool|mixed
     */
    public function getDefault()
    {
        return false;
    }
}
