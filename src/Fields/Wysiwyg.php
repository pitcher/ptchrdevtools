<?php

namespace PtchrProjects\PtchrDevTools\Fields;

use function App\loremipsum;
use PtchrProjects\PtchrDevTools\Lorem;
use Samrap\Acf\Acf;
use StoutLogic\AcfBuilder\FieldsBuilder;

/**
 * Class Wysiwyg.
 */
class Wysiwyg extends BaseField
{
    /**
     * @var string
     */
    public $type = 'wysiwyg';


    /**
     * Wysiwyg constructor.
     * @param string $name
     * @param string $loremipsumsetting
     */
    public function __construct(
        $name = "wysiwyg",
        $label = "Wysiwyg-editor",
        $instructions = "",
        $required = false,
        $default = false
    ) {
        parent::__construct($name, $label, $instructions, $required, $default);

        if (!$default) {
            $this->setDefault(Lorem::loremipsum('all'));
        }
    }

    /**
     * @return mixed|FieldsBuilder
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function build()
    {
        $title = new FieldsBuilder($this->name);
        $title->addWysiwyg($this->name,
            [
                'label' => $this->label,
                'instructions' => $this->getInstructions(),
                'required' => $this->getRequired(),
                'min' => $this->getMin(),
                'max' => $this->getMax(),
                'wrapper' => [
                    'width' => $this->getWidth() ? $this->getWidth() : 60,
                ]

            ]);

        return $title;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return mixed|string
     */

    public function format($data){
        return '<div class="wysiwyg">' . $data . '</div>';
    }


    public function render(): string
    {
        return '<div class="wysiwyg">' . Acf::field($this->name)->default($this->getDefault())->get() . '</div>';
    }

    public function getDefault()
    {
        return Lorem::loremipsum('all');
    }
}
