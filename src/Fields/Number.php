<?php

namespace PtchrProjects\PtchrDevTools\Fields;

use Samrap\Acf\Acf;
use StoutLogic\AcfBuilder\FieldsBuilder;

/**
 * Class Text.
 */
class Number extends BaseField
{

    /**
     * @var string
     */
    public $type = 'number';

    /**
     * Text constructor.
     * @param string $name
     * @param string $label
     * @param string $instructions
     * @param bool $required
     * @param bool $default
     */
    public function __construct(
	    $name = 'number',
        $label = 'Nummer',
        $instructions = "",
        $required = false,
        $default = false)
    {
        parent::__construct($name, $label, $instructions, $required, $default);

	}

    /**
     * @return FieldsBuilder
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function build()
	{
		$title = new FieldsBuilder($this->name);
		$title->addNumber(
		    $this->name,
            [
                'label' => $this->label,
                'wrapper' => [
                    'width' => $this->getWidth()? $this->getWidth() : 20
                ]
            ]);

		return $title;
	}

    /**
     * @return string
     */
    public function render(): string
	{
	    return false;
	}
}
