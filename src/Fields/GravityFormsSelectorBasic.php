<?php

namespace PtchrProjects\PtchrDevTools\Fields;

use Samrap\Acf\Acf;
use StoutLogic\AcfBuilder\FieldsBuilder;

/**
 * Class GravityFormsSelectorBasic.
 */
class GravityFormsSelectorBasic extends BaseField
{
    /**
     * @var string
     */
    public $type = 'gravityformsbasic';
    /**
     * @var array
     */
    private $choices = [];


    /**
     * Radio constructor.
     * @param array $choices
     * @param string $name
     * @param string $label
     */
    public function __construct(
        $name = "gravityformsbasic",
        $label = "Formulier",
        $instructions = "Selecteer een formulier",
        $required = false,
        $default = false
    ) {
        parent::__construct($name, $label, $instructions, $required, $default);
        if (class_exists(\GFAPI::class)) {
            $choices[] = [0 => "geen formulier"];
            foreach (\GFAPI::get_forms() as $form) {
                $choices[] = [ $form['id'] => $form['title'] . " ( #" . $form['id'] . ")"];
            }
            $this->setChoices($choices);
        }
    }

    /**
     * @return mixed|FieldsBuilder
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function build()
    {
        $gravityforms = new FieldsBuilder($this->name);
        $gravityforms->addRadio($this->name,
            [
                'label' => $this->label,
                'wrapper' => [
                    'width' => $this->getWidth() ? $this->getWidth() : 33,
                    'class' => '',
                    'id' => '',
                    'return_format' => 'value',
                ],
                'choices' => $this->choices,
            ]
        );

        return $gravityforms;
    }


    /**
     * @return array
     */
    public function getChoices(): array
    {
        return $this->choices;
    }

    /**
     * @param array $choices
     * @return GravityFormsSelectorBasic
     */
    public function setChoices(array $choices): GravityFormsSelectorBasic
    {
        $this->choices = $choices;
        return $this;
    }

    /**
     * @return mixed
     */
    public function render(): string
    {
        return false;
    }

}
