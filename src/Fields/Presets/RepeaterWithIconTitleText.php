<?php

namespace PtchrProjects\PtchrDevTools\Fields\Presets;


use PtchrProjects\PtchrDevTools\Fields\Image;
use PtchrProjects\PtchrDevTools\Fields\Repeater;
use PtchrProjects\PtchrDevTools\Fields\Text;
use PtchrProjects\PtchrDevTools\Fields\Title;

class RepeaterWithIconTitleText extends Preset
{
    public function __construct()
    {
        parent::__construct();
    }

    static public function get()
    {
        return new Repeater([
            (new Image('icon'))->setSize('four-twenty'),
            new Title(),
            new Text()
        ]);
    }
}
