<?php

namespace PtchrProjects\PtchrDevTools\Fields;


use PtchrProjects\PtchrDevTools\Lorem;
use Samrap\Acf\Acf;
use StoutLogic\AcfBuilder\FieldsBuilder;

/**
 * Class Button.
 */
class Button extends BaseField
{
    /**
     * @var string
     */
    public $type = 'button';


    /**
     * Button constructor.
     * @param string $name
     * @param string $loremipsumsetting
     */
    public function __construct(
        $name = 'button',
        $label = "Knop",
        $instructions = '',
        $required = false,
        $default = false
    ) {
        $this->type = 'button';
        parent::__construct($name, $label, $instructions, $required, $default);

        if (!$default) {
            $this->setDefault(Lorem::loremButton());
        }
    }

    /**
     * @return mixed|FieldsBuilder
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function build()
    {
        $title = new FieldsBuilder($this->getName());
        $title->addLink($this->getName(),
            [
                'label' => $this->getLabel(),
                'instructions' => $this->getInstructions(),
                'required' => $this->getRequired(),
                'wrapper' => [
                    'width' => $this->getWidth()? $this->getWidth() : 25
                ]
            ]);

        return $title;
    }


    public function format($data)
    {
      return false;
    }
}
