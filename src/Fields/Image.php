<?php

namespace PtchrProjects\PtchrDevTools\Fields;

use PtchrProjects\PtchrDevTools\ImgSrcSet;
use Samrap\Acf\Acf;
use StoutLogic\AcfBuilder\FieldsBuilder;

/**
 * Class Image.
 */
class Image extends BaseField
{
    /**
     * @var string
     */

    public $type = 'image';
    /**
     * @var string
     */
    public $size;

    /**
     * @var string
     */
    public $fallbackgroup = "default";

    /**
     * @var string
     */
    public $fit;

    /**
     * @var string
     */
    public $class;

    /**
     * @return string
     */
    public function getReturnFormat(): string
    {
        return $this->return_format;
    }

    /**
     * @param string $return_format
     * @return Image
     */
    public function setReturnFormat(string $return_format): Image
    {
        $this->return_format = $return_format;
        return $this;
    }

    public $return_format = "id";
    /**
     * Image constructor.
     * @param string $name
     * @param string $label
     * @param string $instructions
     * @param bool $required
     * @param bool $default
     * @param string $size
     * @param string $fit
     * @param string $class
     * @throws \Samrap\Acf\Exceptions\BuilderException
     */
    public function __construct(
        $name = "image",
        $label = "Afbeelding",
        $instructions = "Selecteer een afbeelding om te tonen",
        $required = false,
        $default = "https://via.placeholder.com/1000",
        $size = 'ten-eighty',
        $fit = 'cover',
        $class = ''
    ) {
        parent::__construct($name, $label, $instructions, $required, $default);
        $this->size = $size;
        $this->fit = $fit;
        $this->class = $class;

        if (ImgSrcSet\Options::getFallBackImage($this->fallbackgroup)) {
            $this->setDefault(ImgSrcSet\Options::getFallBackImage($this->fallbackgroup));
        } else {
            $this->setDefault($default);
        }

    }

    /**
     * @return mixed|FieldsBuilder
     * @throws \StoutLogic\AcfBuilder\FieldNameCollisionException
     */
    public function build()
    {
        $title = new FieldsBuilder($this->name);
        $title->addImage($this->name,
            [
                'label' => $this->label,
                'return_format' => $this->getReturnFormat(),
                'wrapper' => [
                    'width' => $this->getWidth() ? $this->getWidth() : 33,
                    'class' => '',
                    'id' => '',
                ],
            ]);

        return $title;
    }

    /**
     * @param $data
     * @return bool|mixed|string
     * @throws \Samrap\Acf\Exceptions\BuilderException
     */
    public function format($data)
    {

        // boolean
        // int ( from acf )
        // url ( from repeater )
        // array ?

        // When the data is false, we still call the ImgSrcSet function to get an image, albeit be it a placeholder.
        if (!$data && $this->defaultActive && !ImgSrcSet\Options::getFallBackImage()) {
            return ImgSrcSet::convertImageToSourceSet(
                $this->getDefault(),
                $this->getFit(),
                $this->getClass());
        }

        // When the data is false, we still call the ImgSrcSet function to get an image, albeit be it a placeholder.
        if (!$data && $this->defaultActive && ImgSrcSet\Options::getFallBackImage()) {
            return ImgSrcSet::getImageSourceSet(
                $this->getDefault(),
                $this->getSize(),
                $this->getFit(),
                $this->getClass());
        }

        // If the data is an array, we try to convert the image url back to an image. the url should be the first parameter
        if (is_array($data) && isset($data[0])) {
            return ImgSrcSet::getImageSourceSet(
                $this->get_attachment_id_by_url($data[0]),
                $this->getSize(),
                $this->getFit(),
                $this->getClass());
        }

        // If the data is an int, we assume the source of this is a proper acf field that returns the id of the image.
        if (intval($data)) {
            return ImgSrcSet::getImageSourceSet(
                $data,
                $this->getSize(),
                $this->getFit(),
                $this->getClass());

        }

        // If the data is a string , we try to convert the image url back to an image.
        if (strval($data)) {
            return ImgSrcSet::getImageSourceSet(
                $this->get_attachment_id_by_url($data),
                $this->getSize(),
                $this->getFit(),
                $this->getClass());
        }


    }

    /**
     * @param $url
     * @return int
     */
    function get_attachment_id_by_url($url)
    {
        $post_id = attachment_url_to_postid($url);

        if (!$post_id) {
            $dir = wp_upload_dir();
            $path = $url;
            if (0 === strpos($path, $dir['baseurl'] . '/')) {
                $path = substr($path, strlen($dir['baseurl'] . '/'));
            }

            if (preg_match('/^(.*)(\-\d*x\d*)(\.\w{1,})/i', $path, $matches)) {
                $url = $dir['baseurl'] . '/' . $matches[1] . $matches[3];
                $post_id = attachment_url_to_postid($url);
            }
        }

        return (int)$post_id;
    }


    /**
     * @return string
     */
    public function getFallbackgroup(): string
    {
        return $this->fallbackgroup;
    }

    /**
     * @param string $fallbackgroup
     * @return Image
     */
    public function setFallbackgroup(string $fallbackgroup): Image
    {
        $this->fallbackgroup = $fallbackgroup;
        return $this;
    }

    /**
     * @return string
     */
    public function getSize(): string
    {
        return $this->size;
    }

    /**
     * @param string $size
     * @return Image
     */
    public function setSize(string $size): Image
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @return string
     */
    public function getFit(): string
    {
        return $this->fit;
    }

    /**
     * @param string $fit
     * @return Image
     */
    public function setFit(string $fit): Image
    {
        $this->fit = $fit;
        return $this;
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * @param string $class
     * @return Image
     */
    public function setClass(string $class): Image
    {
        $this->class = $class;
        return $this;
    }

}
