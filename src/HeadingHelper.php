<?php


namespace PtchrProjects\PtchrDevTools;


/**
 * Class HeadingHelper
 * @package App\Helper
 */
class HeadingHelper
{

    /**
     *
     */
    static function initGlobal(){
        global $headingCounter;
        $headingCounter = 0;
    }

    /**
     * @param $text
     * @param int $prefferedHeadingSize
     * @param string $headingclass
     * @return bool|string
     */
    static function Render($text, $prefferedHeadingSize = 1, $headingclass = '')
    {

        $headingclasses = 'title';

        if ($headingclass) {
            $headingclasses .= ' '.$headingclass;
        }
        if (! $text || $text == '') {
            return "";
        }

        if ($prefferedHeadingSize == 1 && self::getGlobal() < 1) {
            self::increment();
            return '<h'.$prefferedHeadingSize." class='".$headingclasses."'>".$text.'</h'.$prefferedHeadingSize.'>';
        }
        if ($prefferedHeadingSize == 1 && self::getGlobal() >= 1) {
            $prefferedHeadingSize = 2;
            return '<h'.$prefferedHeadingSize." class='".$headingclasses."'>".$text.'</h'.$prefferedHeadingSize.'>';
        }

        return '<h'.$prefferedHeadingSize." class='".$headingclasses."'>".$text.'</h'.$prefferedHeadingSize.'>';
    }

    /**
     *
     */
    static function increment(){
        global $headingCounter;
        $headingCounter++;
    }
    /**
     *
     */
    static function getGlobal(){
        global $headingCounter;
        return $headingCounter;
    }

}
