<?php

namespace PtchrProjects\PtchrDevTools;


class GutenbergCategories
{


    function __construct()
    {
        $this->createCallToActionCategory();
        $this->createContentCategory();
        $this->createMediaCategory();
        $this->createHeaderCategory();
        $this->createOtherCategory();
    }

    function createContentCategory()
    {
        add_filter('block_categories', function ($categories, $post) {
            return array_merge(
                $categories,
                array(
                    array(
                        'slug' => 'ptchr-content',
                        'title' => __('📃 Content', 'ptchr-theme'),
                    ),
                )
            );
        }, 10, 2);
    }

    function createMediaCategory()
    {
        add_filter('block_categories', function ($categories, $post) {
            return array_merge(
                $categories,
                array(
                    array(
                        'slug' => 'ptchr-media',
                        'title' => __('📺 Media', 'ptchr-theme'),
                    ),
                )
            );
        }, 10, 2);
    }

    function createCallToActionCategory()
    {
        add_filter('block_categories', function ($categories, $post) {
            return array_merge(
                $categories,
                array(
                    array(
                        'slug' => 'ptchr-call-to-action',
                        'title' => __('📢 Call to Action', 'ptchr-theme'),
                    ),
                )
            );
        }, 10, 2);
    }

    function createHeaderCategory()
    {
        add_filter('block_categories', function ($categories, $post) {
            return array_merge(
                $categories,
                array(
                    array(
                        'slug' => 'ptchr-header',
                        'title' => __('🖼 Headers', 'ptchr-theme'),
                    ),
                )
            );
        }, 10, 2);
    }

    function createOtherCategory()
    {
        add_filter('block_categories', function ($categories, $post) {
            return array_merge(
                $categories,
                array(
                    array(
                        'slug' => 'ptchr-other',
                        'title' => __('🟨 Overig', 'ptchr-theme'),
                    ),
                )
            );
        }, 10, 2);
    }
}
