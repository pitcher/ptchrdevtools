<?php
/**
 * Created by PhpStorm.
 * User: Pitcher - HP
 * Date: 18/09/2020
 * Time: 11:13
 */

namespace PtchrProjects\PtchrDevTools;


class BlockEditorStyling
{
    public function __construct()
    {
        $this->adminCss();
    }

    function adminCss(){
        add_action(
            'admin_footer', function () { ?>
            <style>

                .wp-block {
                    max-width: 95%;
                }

                .acf-block-preview{
                    border: 3px solid #eaeaea;
                    paddinG: 15px 10px 5px 10px;
                }

                .acf-block-preview .class-helper{
                    position: absolute;
                    top: 0;
                    left: 0;
                    z-index: 1;
                    padding: 0.5rem 1rem;
                    color: #000;
                    font-weight: bold;
                    font-size: 12px;
                    line-height: 1;
                    text-transform: uppercase;
                    background: rgba(0, 0, 0, 0.1);
                    border-top-right-radius: 3px;
                    border-bottom-right-radius: 3px;
                }


            </style>
            <?php
        }
        );
    }



}
