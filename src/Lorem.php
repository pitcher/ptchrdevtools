<?php

namespace PtchrProjects\PtchrDevTools;


use PtchrProjects\PtchrDevTools\Lorem\Options;

class Lorem
{

    static function loremipsum($type = 'text')
    {
        $content = '';
        if ($type == 'short') {
            $content = 'Lorem ipsum dolor sit amet.';
        }

        if ($type == 'text') {
            $content = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Parvi enim primo ortu sic iacent, tamquam omnino sine animo sint. Solum praeterea formosum, solum liberum, solum civem, stultost; Sed tu istuc dixti bene Latine, parum plane. Satisne vobis videor pro meo iure in vestris auribus commentatus? Dulce amarum, leve asperum, prope longe, stare movere, quadratum rotundum. Duo Reges: constructio interrete.';
        }

        if ($type == 'all') {
            $content = "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sint modo partes vitae beatae. Nec tamen ullo modo summum pecudis bonum et hominis idem mihi videri potest. Quod idem cum vestri faciant, non satis magnam tribuunt inventoribus gratiam. Age, inquies, ista parva sunt. <a href='http://loripsum.net/' target='_blank'>Duo Reges: constructio interrete.</a> Inde igitur, inquit, ordiendum est. Illa tamen simplicia, vestra versuta. Hi curatione adhibita levantur in dies, valet alter plus cotidie, alter videt. Aliter enim nosmet ipsos nosse non possumus. <b>Egone quaeris, inquit, quid sentiam?</b> </p>
                <ol>
                    <li>Hoc ne statuam quidem dicturam pater aiebat, si loqui posset.</li>
                    <li>Illa sunt similia: hebes acies est cuipiam oculorum, corpore alius senescit;</li>
                    <li>Idem etiam dolorem saepe perpetiuntur, ne, si id non faciant, incidant in maiorem.</li>
                    <li>Qui est in parvis malis.</li>
                    <li>Polemoni et iam ante Aristoteli ea prima visa sunt, quae paulo ante dixi.</li>
                </ol>
                <ul>
                    <li>Mihi, inquam, qui te id ipsum rogavi?</li>
                    <li>Sin autem est in ea, quod quidam volunt, nihil impedit hanc nostram comprehensionem summi boni.</li>
                    <li>Aliter homines, aliter philosophos loqui putas oportere?</li>
                </ul>";
        }

        if (!$content) {
            $content = 'Lorem ipsum dolor sit amet.';
        }
        switch (Options::getLoremSetting()){

            case('blockum'):
                if (Functions::isAdmin() || Functions::is_not_production()) {
                    $char = "█";
                    return '<span class="blocksum placeholder">'.preg_replace('/\\S/', $char, $content) . '</span>';
                }
                break;
            case('false'):
                return false;
                break;
            case('true'):
                return $content;
                break;
            case('default'):
                if (Functions::isAdmin() || Functions::is_not_production()) {
                    return $content;
                }
                break;
            default:
                if (Functions::isAdmin() || Functions::is_not_production()) {
                    return $content;
                }
                break;
                break;
        }

        return false;
    }

    static function loremUsps($items, $title, $content)
    {
        $array = [];
        for ($i = 0; $i < $items; $i++) {
            $array[] = [$title => self::loremipsum('short'), $content => self::loremipsum()];
        }
        if (Functions::isAdmin() || Functions::is_not_production()) {
            return $array;
        }

        return false;
    }

    static function loremButton($text = '')
    {
        $link['url'] = '#';
        $link['title'] = ($text ? $text : self::loremipsum('short'));
        $link['target'] = '';

//        if (ThemeOptions::getLoremSetting() == 'false') {
//            return false;
//        }
//        if (ThemeOptions::getLoremSetting() == 'true') {
//            return Functions::arrayToObject($link);
//        }

        if (Functions::isAdmin() || Functions::is_not_production()) {
            return Functions::arrayToObject($link);
        }

        return false;
    }

    static function loremarray($columns = ['title' => 'short', 'image' => 'image', 'text' => 'text'], $rows = 4)
    {
        $return = [];
        for ($i = 0; $i < $rows; $i++) {
            $rowreturn = [];
            foreach ($columns as $name => $type) {
                switch ($type) {
                    case 'short':
                        $rowreturn[$name] = self::loremipsum('short');
                        break;

                    case 'text':
                        $rowreturn[$name] = self::loremipsum('text');
                        break;

                    case 'wysiwyg':
                        $rowreturn[$name] = self::loremipsum('all');
                        break;

                    case 'image':
                        $rowreturn[$name] = ImgSrcSet::getImageSourceSet(false, 'six-hundred',
                            'contain');
                        break;

                    case 'icon':
                        $rowreturn[$name] = '<img src="https://via.placeholder.com/32" alt="placeholder placeholder--icon">';
                        break;

                    case 'all':
                        $rowreturn[$name] = self::loremipsum('all');
                        break;

                    case 'button':
                        $rowreturn[$name] = self::loremButton();
                        break;

                    default:
                        $rowreturn[$name] = self::loremipsum('short');
                        break;
                }
            }
            $return[] = $rowreturn;
        }

        return $return;
    }


    static function loremArrayRaw($columns, $rows)
    {
        $columns = Functions::arrayToObject($columns);

        $return = [];

        for ($i = 0; $i < $rows; $i++) {
            $rowreturn = [];
            foreach ($columns as $name => $column) {
                $rowreturn[$name] = [
                    'type' => $column->type,
                    'data' => $column->data,
                ];
            }
            $return[] = $rowreturn;
        }

        return $return;
    }

    static function loremFieldStructure($columns, $rows)
    {
        $columns = Functions::arrayToObject($columns);

        $return = [];

        for ($i = 0; $i < $rows; $i++) {
            $rowreturn = [];
            foreach ($columns as $name => $column) {
                $rowreturn[$name] = [
                    'type' => $column->type,
                    'get' => $column->get,
                    'render'=> $column->render
                ];
            }
            $return[] = $rowreturn;
        }

        return $return;
    }
}
