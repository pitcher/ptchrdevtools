<?php

namespace PtchrProjects\PtchrDevTools\Atomic;


use PtchrProjects\PtchrDevTools\Functions;

class Atomic
{
    /**
     * @var string
     */
    private $type = '';

    /**
     * @var array
     */
    private $data = [];

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var array
     */
    private $children = [];

    /**
     * @var array
     */
    private $classes = [];

    /**
     * @var array
     */
    private $errors = [];

    /**
     * @var array
     */
    private $debug = [];

    /**
     * @var array
     */
    private $atoms = [];

    /**
     * @var array
     */
    private $organism = [];

    /**
     * @var array
     */
    private $groups = [];


    function __construct()
    {
        $this->name = "";
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return Atomic
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getData($needle = null, $applyfilters = true)
    {
        if(!$needle){
            if(is_string($this->data)){
                if($applyfilters){
                    return apply_filters('the_content', $this->data);
                }
                if(!$applyfilters){
                    return $this->data;
                }

            }
            return Functions::arrayToObject($this->data);
        }

        foreach($this->data as  $row => $data){
            if(in_array($needle, array_keys($data))){
                return  Functions::arrayToObject($this->data[$row][$needle]);
            }
        }
        $this->AddError('no data found in GetData, looking for'. $needle . ', but nothing found...');
        return false;
    }

    /**
     * @param mixed $data
     * @return Atomic
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @param mixed $data
     * @return Atomic
     */
    public function addData($data)
    {
        $this->data[] = $data;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {

        if ($this->name) {

            return $this->name;

        }

        return false;

    }

    /**
     * @param mixed $name
     * @return Atomic
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }


    public function hasChildren()
    {
        if ($this->getChildren() && count($this->getChildren()) > 0) {
            return true;
        }
        return false;
    }

    /**
     * @param mixed $children
     * @return Atomic
     */
    public function addChild($child)
    {
        $this->children[] = $child;
        return $this;
    }


    /**
     * @param mixed $children
     * @return Atomic
     */
    public function addChildren($array)
    {
        if (!is_array($array)) {
            $array[] = $array;
        }

        foreach ($array as $a) {
            $this->addChild($a);
        }
        return $this;
    }


    /**
     * @return mixed
     */
    public function getClasses()
    {
        return $this->classes;
    }

    /**
     * @param mixed $class
     * @return Atomic
     */
    public function setClass($class)
    {
        $this->classes = $class;
        return $this;
    }

    /**
     * @param mixed $classes
     * @return Atomic
     */
    public function addClass($classes)
    {
        $this->classes[] = $classes;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFormattedClasses()
    {

        if (!$this->getClasses()) {
            return "";
        }
        $classes = "";

        if (is_array($this->getClasses())) {
            $classes = "";
            foreach ($this->getClasses() as $class) {
                $classes .= " " . $class;
            }
        }

        if (!is_array($this->getClasses())) {
            $classes = $this->getClasses();
        }


        return 'class="' . $classes . '"';

    }


    /**
     * @return mixed
     */
    public function getDebugs()
    {
        return $this->debug;
    }

    /**
     * @param mixed $errors
     * @return Atomic
     */
    public function AddDebug($debugmessage)
    {
        $this->debug[] = $debugmessage;
        return $this;
    }

    /**
     * @return Atomic
     */

    public function debug()
    {
        $return = [];



        return $this;
    }


    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     * @return Atomic
     */
    public function AddError($error)
    {
        $this->errors[] = $error;
        return $this;
    }

    /**
     * @param mixed $needle $haystack
     * @return Atomic
     */

    public function findAtomic($needle, $haystack)
    {
        foreach ($haystack->getChildren() as $child) {

            if ($child->getName() === $needle) {
                return $child;
            } elseif (is_array($child->getChildren())) {
                $result = $this->findAtomic($needle, $child);
                if ($result !== false) {
                    return $result;
                }
            }
        }

        return false;
    }


    /**
     * @param mixed $atomname
     * @return Atomic
     */
    public function getAtomic($needle)
    {

        return $this->findAtomic($needle, $this);

    }

    /**
     * @return mixed
     */
    public function getAtoms()
    {
        return $this->atoms;
    }

    /**
     * @param mixed $atom
     * @return Atomic
     */
    public function addAtom(Atom $atom)
    {
        $this->atoms[] = $atom;
        $this->addChild($atom);
        return $this;
    }



    public function validate()
    {

        return $this;
    }

}


