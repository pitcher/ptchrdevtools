<?php


namespace PtchrProjects\PtchrDevTools\Controllers;

use PtchrProjects\PtchrDevTools\Models\Model;
use PtchrProjects\PtchrDevTools\Resources\DefaultResource;

/**
 * Class Taxonomy
 * @package App\Controllers
 */
class Taxonomy
{
    /**
     * @var Model
     */
    public $model;
    /**
     * @var
     */
    public $taxonomy;

    public $resource;

    public $multilingual = false;

    public $suppress = true;

    /**
     * Taxonomy constructor.
     * @param Model $model
     * @param $taxonomy
     */
    public function __construct(Model $model, $taxonomy)
    {
        $this->model = $model;
        $this->taxonomy = $taxonomy;
        $this->resource = new DefaultResource();
        $this->multilingual = false;
    }

    /**
     * @return bool
     */
    public function isIsmultilingual(): bool
    {
        return $this->multilingual;
    }

    /**
     * @param bool $ismultilingual
     * @return Controller
     */
    public function setIsmultilingual(bool $ismultilingual): Taxonomy
    {
        $this->ismultilingual = $ismultilingual;

        if($ismultilingual){
            $this->suppress = false;
        }

        return $this;
    }


    /**
     * @param mixed $resource
     */
    public function setResource($resource): void
    {
        $this->resource = $resource;
    }

    /**
     * @return Model
     */
    public function getModel(): Model
    {
        return $this->model;
    }

    /**
     * @param Model $model
     * @return Taxonomy
     */
    public function setModel(Model $model): Taxonomy
    {
        $this->model = $model;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTaxonomy()
    {
        return $this->taxonomy;
    }

    /**
     * @param mixed $taxonomy
     * @return Taxonomy
     */
    public function setTaxonomy($taxonomy)
    {
        $this->taxonomy = $taxonomy;
        return $this;
    }

    public function getAll()
    {
        $posts = get_terms([
                'taxonomy' => $this->getTaxonomy()->taxonomytype,
                'hide_empty' => false,
                'post_type' => $this->getModel()->type,
                'suppress_filters' => $this->suppress
            ]
        );

        return $this->resource->Mapping($posts);
    }


}
