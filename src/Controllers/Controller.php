<?php

namespace PtchrProjects\PtchrDevTools\Controllers;


use PtchrProjects\PtchrDevTools\Models\Model;
use PtchrProjects\PtchrDevTools\Resources\DefaultResource;
use PtchrProjects\PtchrDevTools\Resources\DefaultTermFilterResource;
use PtchrProjects\PtchrDevTools\Functions;

class Controller
{
    public $model;
    public $resource;
    public $ismultilingual = false;
    public $suppress = true;

    public function __construct(Model $model)
    {
        $this->model = $model;
        $this->resource = new DefaultResource;

    }

    /**
     * @return bool
     */
    public function isIsmultilingual(): bool
    {
        return $this->ismultilingual;
    }

    /**
     * @param bool $ismultilingual
     * @return Controller
     */
    public function setIsmultilingual(bool $ismultilingual): Controller
    {
        $this->ismultilingual = $ismultilingual;

        if($ismultilingual){
            $this->suppress = false;
        }

        return $this;
    }


    /**
     * @param mixed $resource
     */
    public function setResource($resource): void
    {
        $this->resource = $resource;
    }

    public function getAll($posts_per_page = 100)
    {
        $posts = get_posts([
            'post_type' => $this->model->getType(),
            'posts_per_page' => $posts_per_page,
            'suppress_filters' => $this->suppress
        ]);

        return $this->resource->Mapping($posts);
    }

    public function getAllWithoutMapping($posts_per_page = 100)
    {
        return get_posts([
            'post_type' => $this->model->getType(),
            'posts_per_page' => $posts_per_page,
            'suppress_filters' =>  $this->suppress
        ]);
    }

    public function getAllHierachical($posts_per_page = 100, $hide_empty = false)
    {

        $posts = $this->getAllParents($posts_per_page);

        foreach ($posts as $key => $post) {

            $args = array(
                'post_type' => $this->model->getType(),
                'posts_per_page' => -1,
                'post_parent' => $post->ID,
                'suppress_filters' => $this->suppress
            );

            $children = get_posts($args);
            $posts[$key]->children = $this->resource->Mapping($children);  //returns Array ( [$image_ID].
            if ($hide_empty) {
                if (!$children) {
                    unset($posts[$key]);
                }
            }
        }

        return Functions::arrayToObject($posts);
    }


    public function getAllParents($posts_per_page = 100)
    {

        $posts = get_posts([
            'post_type' => $this->model->getType(),
            'posts_per_page' => $posts_per_page,
            'post_parent' => 0,
            'suppress_filters' => $this->suppress
        ]);

        return $this->resource->Mapping($posts);
    }

    public function getAllByParent($parent, $posts_per_page = 100)
    {
        $posts = get_posts([
            'post_type' => $this->model->getType(),
            'posts_per_page' => $posts_per_page,
            'post_parent' => $parent,
            'suppress_filters' => $this->suppress
        ]);

        return $this->resource->Mapping($posts);
    }


    public function getByID(int $id)
    {
        $post = get_post($id);
        $posts[] = $post;
        $mapped = $this->resource->Mapping($posts);
        return $mapped[0];
    }

    public function getAllWithID(array $ids)
    {
        $posts = get_posts([
            'post_type' => $this->model->getType(),
            'posts_per_page' => -1,
            'post__in' => $ids
        ]);

        return $this->resource->Mapping($posts);
    }

    public function getAllByAcfField($field, $search, $limit = 100)
    {

        $args = [
            'post_type' => $this->model->getType(),
            'posts_per_page' => $limit,
            'meta_key' => $field,
            'meta_value' => $search,
            'suppress_filters' => $this->suppress
        ];

        return $this->resource->Mapping(get_posts($args));
    }

    public function paginated($posts_per_page = 15, $page = 1)
    {
        $page = intval($page);

        $query = [];

        parse_str($_SERVER['QUERY_STRING'], $query);

        if (isset($query['pagination'])) {
            $page = intval($query['pagination']);
        }

        $posts_per_page = intval($posts_per_page);

        $postOffset = ($page - 1) * $posts_per_page;

        $total = intval(wp_count_posts($this->model->getType())->publish);

        $posts = get_posts([
            'post_type' => $this->model->getType(),
            'posts_per_page' => $posts_per_page,
            'offset' => $postOffset,
            'suppress_filters' => $this->suppress
        ]);

        $url = explode("?", $_SERVER['REQUEST_URI']);
        $url = $url[0];

        $prev = false;
        if ($page > 1) {
            $prev = $url . "?pagination=" . strval($page - 1);
        }

        $next = false;
        if ($total > $postOffset + $posts_per_page) {
            $next = $url . "?pagination=" . strval($page + 1);
        }

        return [
            'data' => [
                'meta' => [
                    'post_type' => $this->model->getType(),
                    'posts_per_page' => $posts_per_page,
                    'offset' => $postOffset,
                    'page' => $page,
                    'total' => $total
                ],
                'posts' => $posts,
                'pagination' => [
                    'total' => $total,
                    'prev' => $prev,
                    'current' => $url . "?pagination=" . strval($page),
                    'next' => $next
                ]
            ]
        ];
    }

    /**
     * @return Model
     */
    public function getModel(): Model
    {
        return $this->model;
    }

    /**
     * @param Model $model
     */
    public function setModel(Model $model): void
    {
        $this->model = $model;
    }


    public function getByCategoryId($category, $categoryId)
    {
        $args = array(
            'posts_per_page' => -1,
            'suppress_filters' => $this->suppress,
            'post_type' => $this->model->getType(),
            'tax_query' => array(
                array(
                    'taxonomy' => $category,
                    'field' => 'term_id',
                    'terms' => $categoryId,
                )
            )
        );

        return $this->resource->Mapping(get_posts($args));
    }

    public function getFilteredByCategoryId($filterslug, $category)
    {
        $allCategories = (new DefaultTermFilterResource())->Mapping(get_terms([
            'taxonomy' => $category,
            'post_type' => $this->getModel()->getType(),
            'hide_empty' => false
        ]));

        $data['filters'] = $allCategories;

        parse_str($_SERVER['QUERY_STRING'], $query);

        if (isset($query[$filterslug])) {
            $id = intval($query[$filterslug]);
            $data['items'] = $this->getByCategoryId($category, $id);
        } else {
            $data['items'] = $this->getAll();
        }

        return $data;


    }

}
