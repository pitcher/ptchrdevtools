<?php
/**
 * Created by PhpStorm.
 * User: Pitcher - HP
 * Date: 05/10/2020
 * Time: 12:26
 */

namespace PtchrProjects\PtchrDevTools;


class removeDashboardSiteHealthWidget
{
    public function __construct()
    {
        add_action('wp_dashboard_setup', function(){
            remove_meta_box('dashboard_site_health', 'dashboard', 'normal');
        });
    }
}
